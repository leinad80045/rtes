![](Media/Icon/Logo_Rescue.png "Rescue")

# Rescu-e

**Rescu-e** is a project developed at the University of Glasgow. The main of rescu-e is to help to find traped people in case of a collapsed building.

**What does differenciate Rescu-e from its alternatives?**

The main difference is that rescu-e is focused on finding people, for this reason, we've prioritized the agility. Its dimensions are very reduced. It happens the same with the weigh. It has 4 wheel drive to not get block and move with the need of the minimum space. With the camera, which has independent movement, we can see even in the darkest situations thanks to its night vision. Last but not least, rescu-e does not have a special controller, it has a mobile app to control all the features and can be installed on an android phone. With this app, we avoid using a physical connection between the controller and the robot as most of the alternatives have.
We are very proud to have a project that can be easily reproduced with a small budget.

<div align="center">

![](Media/Rescu_e%20photos/IMG_20200401_132558%20-%20copia.jpg)

</div>


**Features of this project:**
- Mobile application based on Android system with control button and real-time images from the camera.
- Forward, turn, spin function.
- Freely rotating camera.
- Microphone for monitoring potential personnel.
- Rechargeable battery.

**Objectives**
- Servo and DC drivers for motor
- Develop the data packages to control the robot
- Use threads to control the server data request 
- Get the sampling data from the microphones using the new MCP3004

**Rationale**

Rescu-e was born due to the deficiencies found in its alternatives. This project aims to provide support in situations as difficult and complicated as a landslide. In these cases it works against the clock.
Nor can we ignore that this is an academic project. With which it is intended to expand knowledge in Real-time programming.


# Documentation
Complete guides on how to build and use the infotaiment system can be found on our [wiki](https://gitlab.com/Leinadserra/rtes/-/wikis/home). Detailed software documentation can be found in the [Doxygen](https://leinad80045.gitlab.io/rtes/) documentation.

 
# Social media

Follow us on our social media pages to see the project evolution with all the changes, tests and problems faced, and send us an [email](mailto:rescu.e.uofg@gmail.com) if you want to contact us.

<div align="center">

[![Facebook](Media/Icon/descarga.png)](https://www.facebook.com/Rescu.e.uofg/)          [![Twitter](Media/Icon/60580.png)](https://twitter.com/rescu_e)             [![Youtube](Media/Icon/1200px-YouTube_dark_icon_(2017).svg.png)](https://www.youtube.com/channel/UCL0QOqNy35aUSwtc1hH106w?view_as=subscriber) [![Hackaday](Media/Icon/8Pm7yZFb.png)](https://hackaday.io/project/171000-rescu-e-the-robot-to-locate-people-in-collapses)

</div>


# Authors

Rescu-e has been develop by a group of 3 master students from the University of Glasgow, each of us had different task. Check more about us in our wiki pages (clicking in our names)

[Daniel](https://gitlab.com/Leinadserra/rtes/-/wikis/Authors-and-responsabilities#xinrui-liu) - Hardware 

[Xinrui Liu](https://gitlab.com/Leinadserra/rtes/-/wikis/Authors-and-functions#xinrui-liu) - Software

[Alberto](https://gitlab.com/Leinadserra/rtes/-/wikis/Authors-and-responsabilities#alberto-naranjo) - Mobile app

Supervissed: [Bernd Porr](https://github.com/berndporr)
