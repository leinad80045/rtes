package com.example.servocontrollerwifi;

        import androidx.appcompat.app.AppCompatActivity;
        import android.annotation.SuppressLint;
        import android.content.DialogInterface;
        import android.os.AsyncTask;
        import android.os.Bundle;
        import android.view.MotionEvent;
        import android.view.View;
        import android.widget.Button;
        import android.widget.EditText;
        import android.webkit.WebView;
        import android.webkit.WebViewClient;
        import android.widget.ImageButton;
        import android.widget.TextView;
        import java.io.DataOutputStream;
        import java.io.IOException;
        import java.net.InetAddress;
        import java.net.Socket;
        import java.net.UnknownHostException;
        import io.github.controlwear.virtual.joystick.android.JoystickView;
        import android.app.AlertDialog;
        import android.widget.Toast;


/**
 * The Rescu-E program is used to control Rescu-e robot with the phone
 * Tipping the IP address we establish the hotspot connection.
 * to move the robot we can use the joysticks
 * at the background the robot view is displayed
 *
 * @author  Alberto Naranjo
 * @version 1.0
 * @since   2020
 */

public class HomeActivity extends AppCompatActivity {

    private EditText txtAddress; //!<Gener  al IP Address
    public static String wifiModuleIp = ""; //!<empty variable for IP address (movement)
    public static int wifiModulePort = 0; //!<empty variable for ports (movement)
    public static String wifiModuleIp2 = ""; //empty variable for IP address (stream)
    public static int wifiModulePort2 = 0; //empty variable for ports (stream)
    public static String ANG = "0"; //string vehicle movement
    public static String STE = "OFF"; //initial robot state
    public static String CMD = "9"; //string camera movement
    private WebView webView; //Broadcast viewer
    private TextView mTextViewAngleMovement; //joystick movement direction
    private TextView mTextViewStrengthMovement; //joystick movement intensity
    private TextView mTextViewAngleCamera; //joystick camera direction
    private ImageButton closeBtn;
    AlertDialog.Builder builder;

    public void vista(View reloadbtn){ //Reload the webview with the new IP address on the edittext
        /**
         * This method is used to reload the webview
         * @param reloadbtn This is the first parameter
         */
        String iP = txtAddress.getText().toString();
        webView.loadUrl("http://"+iP+":8081"); //IP Address with port for video stream
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) { //immersive mode, hide tool and navigation bars
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    private void hideSystemUI() {
        // Enables regular immersive mode.
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    // Shows the system bars by removing all the flags
    // except for the ones that make the content appear under the system bars.
    private void showSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        txtAddress = (EditText) findViewById(R.id.ipAddress); //Edit text to introduce the IP Address
        webView = (WebView) findViewById(R.id.webview); //A webView loads an specifics IP address where the broadcast it is being send
        String iP = txtAddress.getText().toString(); //transforms into string the IP address
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl("http://"+iP+":8081"); //IP Address with port for video stream http://192.168.43.146:8000

        webView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        }); //disable webpage interaction, the video size comes from the rpi

        mTextViewAngleCamera = (TextView) findViewById(R.id.textView_angle_camera); //Test to know the angle in 360ª of the joystick
        mTextViewAngleMovement = (TextView) findViewById(R.id.textView_angle_movement); //Test to know the angle in 360ª of the joystick
        mTextViewStrengthMovement = (TextView) findViewById(R.id.textView_strength_movement); //test to know the distance from the center 0-center 100-max

        //CAMERA MOVEMENT CODE
        final JoystickView joystickCamera = (JoystickView) findViewById(R.id.joystickView_camera);
        joystickCamera.setOnMoveListener(new JoystickView.OnMoveListener() {
            @SuppressLint("DefaultLocale")
            @Override
            public void onMove(int angle, int strength) {

                /**
                 * This method is used to move the camera
                 * of the robot, it sends a letter depending on the position
                 * M-up; N-left; O-down; P-right; nothing-Z
                 * @param joystickView_camera This is the parameter to move the camera
                 */

                mTextViewAngleCamera.setText(angle + "°"); //Shows the joystick position in degrees

                //To make the movement easier, the whole circle has been divided in 4 direction areas
                //Each combination has a letter to send only 1 character

                //M-up; N-left; O-down; P-right; nothing-Z

                if (angle <= 45 && strength > 0){
                    CMD = "P";
                } else if (angle > 45 && angle <= 135){
                    CMD = "M";
                } else if (angle > 135 && angle <= 225){
                    CMD = "N";
                } else if (angle > 225 && angle <= 315){
                    CMD = "O";
                } else if (angle > 315){
                    CMD = "P";
                } else {
                    CMD = "Z";
                }
                getIP2();
                Socket2_AsyncTask cmd_increase_servo2 = new Socket2_AsyncTask();
                cmd_increase_servo2.execute();
            }
        },80); //the loop interval is low to not collapse the socket, for the camera movement smoothness 80 is enough

        //CAR MOVEMENT CODE
        final JoystickView joystickRight = (JoystickView) findViewById(R.id.joystickView_movement);
        joystickRight.setOnMoveListener(new JoystickView.OnMoveListener() {
            @SuppressLint("DefaultLocale")
            @Override
            public void onMove(int angle, int strength) {

                /**
                 * This method is used to move the robot
                 * it sends a letter depending on the position
                 * Direction areas: 315º - 45º right
                 * 45º - 135º forward
                 * 35º - 225º left
                 * 225º - 315º backward
                 *
                 * 1-32% - level 1 strength
                 * 33% - 65% - level 2 strength
                 * 66% - 100% - level 3 strength
                 * @param joystickView_movement This is the parameter to move the camera
                 */

                mTextViewAngleMovement.setText(angle + "°"); //Shows the joystick position in degrees
                mTextViewStrengthMovement.setText(strength + "%"); //shows how far is the joystick

                //To make the movement easier, the whole circle has been divided in 4 direction areas and 3 intensity grades
                //Each combination has a letter to send only 1 character

                //Direction areas: 315º - 45º right
                //45º - 135º forward
                //135º - 225º left
                //225º - 315º backward

                //1-32% - level 1 strength
                //33% - 65% - level 2 strength
                //66% - 100% - level 3 strength
                //center sends 0

                if (angle <= 45 && strength > 0 && strength < 33){
                    ANG = "A";
                } else if (angle <= 45 && strength >= 33 && strength < 66){
                    ANG = "B";
                } else if (angle <= 45 && strength >= 66){
                    ANG = "C";
                } else if (angle > 45 && angle <= 135 && strength > 0 && strength < 33){
                    ANG = "D";
                } else if (angle > 45 && angle <= 135 && strength >= 33 && strength < 66){
                    ANG = "E";
                } else if (angle > 45 && angle <= 135 && strength >= 66){
                    ANG = "F";
                } else if (angle > 135 && angle <= 225 && strength > 0 && strength < 33){
                    ANG = "G";
                } else if (angle > 135 && angle <= 225 && strength >= 33 && strength < 66){
                    ANG = "H";
                } else if (angle > 135 && angle <= 225 && strength >= 66){
                    ANG = "I";
                } else if (angle > 225 && angle <= 315 && strength > 0 && strength < 33){
                    ANG = "J";
                } else if (angle > 225 && angle <= 315 && strength >= 33 && strength < 66){
                    ANG = "K";
                } else if (angle > 135 && angle <= 315 && strength >= 66){
                    ANG = "L";
                } else if (angle > 315 && strength > 0 && strength < 33){
                    ANG = "A";
                } else if (angle > 315 && strength >= 33 && strength < 66){
                    ANG = "B";
                } else if (angle > 315 && strength >= 66){
                    ANG = "C";
                } else {
                    ANG = "0";
                }
                getIP();
                //ANG = Integer.toString(strength); //test to sent an integer instead of an string
                Socket_AsyncTask cmd_increase_servo = new Socket_AsyncTask();
                cmd_increase_servo.execute();
            }
        },100); //the loop interval is low to not collapse the socket, in our case with 100 is enough

        // TURN OFF THE ROBOT
        closeBtn = (ImageButton) findViewById(R.id.closeBtn);
        builder = new AlertDialog.Builder(this);
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Uncomment the below code to Set the message and title from the strings.xml file
                builder.setMessage(R.string.dialog_message) .setTitle(R.string.dialog_title);

                //Setting message manually and performing action on button click
                builder.setMessage("Do you want to turn off RESCU-E and close the APP?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                getIP2();
                                STE = "V"; //updates the variable STE
                                Socket3_AsyncTask cmd_increase_servo2 = new Socket3_AsyncTask(); //updates the socket
                                cmd_increase_servo2.execute();
                                finish();
                                finishAffinity();
                                Toast.makeText(getApplicationContext(),"You choose yes",
                                        Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //  Action for 'NO' Button
                                dialog.cancel();
                                Toast.makeText(getApplicationContext(),"You choose no action",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                //Creating dialog box
                AlertDialog alert = builder.create();
                //Setting the title manually
                alert.setTitle("RESCU-E");
                alert.show();
            }
        });
    }

    //get the IP from the Edit text
    public void getIP() //read the IP ADDRESS and PORT for the Joystick1 (vehicle movement)
    {
        String iP = txtAddress.getText().toString();
        wifiModuleIp = iP;
        wifiModulePort = 21567; //Port for vehicle movement
    }
    public void getIP2(){ //read the IP ADDRESS and PORT for the Joystick2 (camera movement)
        String iP2 = txtAddress.getText().toString();
        wifiModuleIp2 = iP2;
        wifiModulePort2 = 21567; //port for camera movement
    }

    //SOCKET FOR ROBOT MOVEMENT
    public class Socket_AsyncTask extends AsyncTask<Void,Void,Void> { //Send de variable in Bytes through the socket
        Socket socket;
        @Override
        protected Void doInBackground(Void... params){
            try{
                InetAddress inetAddress = InetAddress.getByName(HomeActivity.wifiModuleIp);
                socket = new java.net.Socket(inetAddress,HomeActivity.wifiModulePort);
                DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
                dataOutputStream.writeBytes(ANG); //determine how we want to sent the variable and sends ANG in string format
                dataOutputStream.close();
                socket.close();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e){
                e.printStackTrace();
            } return null;
        }
    }

    //SOCKET FOR CAMERA MOVEMENT
    public class Socket2_AsyncTask extends AsyncTask<Void,Void,Void> { //Send de variable in Bytes through the socket
        Socket socket2;
        @Override
        protected Void doInBackground(Void... params){
            try{
                InetAddress inetAddress2 = InetAddress.getByName(HomeActivity.wifiModuleIp2);
                socket2 = new java.net.Socket(inetAddress2,HomeActivity.wifiModulePort2);
                DataOutputStream dataOutputStream2 = new DataOutputStream(socket2.getOutputStream());
                dataOutputStream2.writeBytes(CMD); //determine how we want to sent the variable and sends CMD in string format
                dataOutputStream2.close();
                socket2.close();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e){
                e.printStackTrace();
            } return null;
        }
    }

    //SOCKET FOR ROBOT ESTATE
    public class Socket3_AsyncTask extends AsyncTask<Void,Void,Void> { //Send de variable in Bytes through the socket
        Socket socket2;
        @Override
        protected Void doInBackground(Void... params){
            try{
                InetAddress inetAddress2 = InetAddress.getByName(HomeActivity.wifiModuleIp2);
                socket2 = new java.net.Socket(inetAddress2,HomeActivity.wifiModulePort2);
                DataOutputStream dataOutputStream2 = new DataOutputStream(socket2.getOutputStream());
                dataOutputStream2.writeBytes(STE); //determine how we want to sent the variable and sends STE in string format
                dataOutputStream2.close();
                socket2.close();
            } catch (UnknownHostException e)
            {
                e.printStackTrace();
            } catch (IOException e){
                e.printStackTrace();
            } return null;
        }
    }
}

