<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.0.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="dots" multiple="1" display="yes" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="Arduino-clone">
<description>Arduino Clone pinheaders
By cl@xganon.com
http://www.xganon.com</description>
<packages>
<package name="NANO">
<pad name="TX0" x="7.62" y="-17.78" drill="1.016" diameter="1.778"/>
<pad name="RX1" x="7.62" y="-15.24" drill="1.016" diameter="1.778"/>
<pad name="RST1" x="7.62" y="-12.7" drill="1.016" diameter="1.778"/>
<pad name="GND1" x="7.62" y="-10.16" drill="1.016" diameter="1.778"/>
<pad name="D2" x="7.62" y="-7.62" drill="1.016" diameter="1.778"/>
<pad name="D3" x="7.62" y="-5.08" drill="1.016" diameter="1.778"/>
<pad name="D4" x="7.62" y="-2.54" drill="1.016" diameter="1.778"/>
<pad name="D5" x="7.62" y="0" drill="1.016" diameter="1.778"/>
<pad name="D6" x="7.62" y="2.54" drill="1.016" diameter="1.778"/>
<pad name="D7" x="7.62" y="5.08" drill="1.016" diameter="1.778"/>
<pad name="D8" x="7.62" y="7.62" drill="1.016" diameter="1.778"/>
<pad name="D9" x="7.62" y="10.16" drill="1.016" diameter="1.778"/>
<pad name="RAW" x="-7.62" y="-17.78" drill="1.016" diameter="1.778"/>
<pad name="GND" x="-7.62" y="-15.24" drill="1.016" diameter="1.778"/>
<pad name="RST" x="-7.62" y="-12.7" drill="1.016" diameter="1.778"/>
<pad name="A3" x="-7.62" y="2.54" drill="1.016" diameter="1.778"/>
<pad name="A2" x="-7.62" y="5.08" drill="1.016" diameter="1.778"/>
<pad name="A1" x="-7.62" y="7.62" drill="1.016" diameter="1.778"/>
<pad name="A0" x="-7.62" y="10.16" drill="1.016" diameter="1.778"/>
<pad name="D13" x="-7.62" y="17.78" drill="1.016" diameter="1.778"/>
<pad name="D12" x="7.62" y="17.78" drill="1.016" diameter="1.778"/>
<pad name="D11" x="7.62" y="15.24" drill="1.016" diameter="1.778"/>
<pad name="D10" x="7.62" y="12.7" drill="1.016" diameter="1.778"/>
<pad name="3.3V" x="-7.62" y="15.24" drill="1.016" diameter="1.778" rot="R180"/>
<pad name="AREF" x="-7.62" y="12.7" drill="1.016" diameter="1.778" rot="R180"/>
<pad name="5V" x="-7.62" y="-10.16" drill="1.016" diameter="1.778"/>
<pad name="A4" x="-7.62" y="0" drill="1.016" diameter="1.778" rot="R180"/>
<pad name="A5" x="-7.62" y="-2.54" drill="1.016" diameter="1.778" rot="R180"/>
<pad name="A6" x="-7.62" y="-5.08" drill="1.016" diameter="1.778" rot="R180"/>
<pad name="A7" x="-7.62" y="-7.62" drill="1.016" diameter="1.778" rot="R180"/>
<text x="-8.19" y="21.38" size="1.778" layer="25">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="NANO">
<pin name="3.3V" x="-22.86" y="12.7" visible="pin" length="middle"/>
<pin name="AREF" x="-22.86" y="10.16" visible="pin" length="middle"/>
<pin name="TX0" x="10.16" y="-20.32" visible="pin" length="middle" rot="R180"/>
<pin name="RX1" x="10.16" y="-17.78" visible="pin" length="middle" rot="R180"/>
<pin name="RST1" x="10.16" y="-15.24" visible="pin" length="middle" rot="R180"/>
<pin name="GND2" x="10.16" y="-12.7" visible="pin" length="middle" rot="R180"/>
<pin name="D2" x="10.16" y="-10.16" visible="pin" length="middle" rot="R180"/>
<pin name="D3" x="10.16" y="-7.62" visible="pin" length="middle" rot="R180"/>
<pin name="D4" x="10.16" y="-5.08" visible="pin" length="middle" rot="R180"/>
<pin name="D5" x="10.16" y="-2.54" visible="pin" length="middle" rot="R180"/>
<pin name="D6" x="10.16" y="0" visible="pin" length="middle" rot="R180"/>
<pin name="D7" x="10.16" y="2.54" visible="pin" length="middle" rot="R180"/>
<pin name="D8" x="10.16" y="5.08" visible="pin" length="middle" rot="R180"/>
<pin name="D9" x="10.16" y="7.62" visible="pin" length="middle" rot="R180"/>
<pin name="RAW" x="-22.86" y="-20.32" visible="pin" length="middle"/>
<pin name="GND" x="-22.86" y="-17.78" visible="pin" length="middle"/>
<pin name="RST" x="-22.86" y="-15.24" visible="pin" length="middle"/>
<pin name="5.5V" x="-22.86" y="-12.7" visible="pin" length="middle"/>
<pin name="A3" x="-22.86" y="0" visible="pin" length="middle"/>
<pin name="A2" x="-22.86" y="2.54" visible="pin" length="middle"/>
<pin name="A1" x="-22.86" y="5.08" visible="pin" length="middle"/>
<pin name="A0" x="-22.86" y="7.62" visible="pin" length="middle"/>
<pin name="D13" x="-22.86" y="15.24" visible="pin" length="middle"/>
<pin name="D12" x="10.16" y="15.24" visible="pin" length="middle" rot="R180"/>
<pin name="D11" x="10.16" y="12.7" visible="pin" length="middle" rot="R180"/>
<pin name="D10" x="10.16" y="10.16" visible="pin" length="middle" rot="R180"/>
<pin name="A7" x="-22.86" y="-10.16" visible="pin" length="middle"/>
<pin name="A6" x="-22.86" y="-7.62" visible="pin" length="middle"/>
<pin name="A5" x="-22.86" y="-5.08" visible="pin" length="middle"/>
<pin name="A4" x="-22.86" y="-2.54" visible="pin" length="middle"/>
<wire x1="-17.78" y1="17.78" x2="-17.78" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-30.48" x2="5.08" y2="-30.48" width="0.254" layer="94"/>
<wire x1="5.08" y1="-30.48" x2="5.08" y2="17.78" width="0.254" layer="94"/>
<wire x1="5.08" y1="17.78" x2="-17.78" y2="17.78" width="0.254" layer="94"/>
<text x="-17.78" y="20.32" size="1.778" layer="95">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="NANO">
<description>Arduino Nano</description>
<gates>
<gate name="G$1" symbol="NANO" x="7.62" y="2.54"/>
</gates>
<devices>
<device name="" package="NANO">
<connects>
<connect gate="G$1" pin="3.3V" pad="3.3V"/>
<connect gate="G$1" pin="5.5V" pad="5V"/>
<connect gate="G$1" pin="A0" pad="A0"/>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="A3" pad="A3"/>
<connect gate="G$1" pin="A4" pad="A4"/>
<connect gate="G$1" pin="A5" pad="A5"/>
<connect gate="G$1" pin="A6" pad="A6"/>
<connect gate="G$1" pin="A7" pad="A7"/>
<connect gate="G$1" pin="AREF" pad="AREF"/>
<connect gate="G$1" pin="D10" pad="D10"/>
<connect gate="G$1" pin="D11" pad="D11"/>
<connect gate="G$1" pin="D12" pad="D12"/>
<connect gate="G$1" pin="D13" pad="D13"/>
<connect gate="G$1" pin="D2" pad="D2"/>
<connect gate="G$1" pin="D3" pad="D3"/>
<connect gate="G$1" pin="D4" pad="D4"/>
<connect gate="G$1" pin="D5" pad="D5"/>
<connect gate="G$1" pin="D6" pad="D6"/>
<connect gate="G$1" pin="D7" pad="D7"/>
<connect gate="G$1" pin="D8" pad="D8"/>
<connect gate="G$1" pin="D9" pad="D9"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="GND2" pad="GND1"/>
<connect gate="G$1" pin="RAW" pad="RAW"/>
<connect gate="G$1" pin="RST" pad="RST"/>
<connect gate="G$1" pin="RST1" pad="RST1"/>
<connect gate="G$1" pin="RX1" pad="RX1"/>
<connect gate="G$1" pin="TX0" pad="TX0"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rescue">
<packages>
<package name="PDIP16">
<pad name="P$1" x="-3.5" y="9" drill="0.8128"/>
<pad name="P$2" x="-3.5" y="6.46" drill="0.8128"/>
<pad name="P$3" x="-3.5" y="3.92" drill="0.8128"/>
<pad name="P$4" x="-3.5" y="1.38" drill="0.8128"/>
<pad name="P$5" x="-3.5" y="-1.16" drill="0.8128"/>
<pad name="P$6" x="-3.5" y="-3.7" drill="0.8128"/>
<pad name="P$7" x="-3.5" y="-6.24" drill="0.8128"/>
<pad name="P$8" x="-3.5" y="-8.78" drill="0.8128"/>
<pad name="P$9" x="4.12" y="-8.78" drill="0.8128"/>
<pad name="P$10" x="4.12" y="-6.24" drill="0.8128"/>
<pad name="P$11" x="4.12" y="-3.7" drill="0.8128"/>
<pad name="P$12" x="4.12" y="-1.16" drill="0.8128"/>
<pad name="P$13" x="4.12" y="1.38" drill="0.8128"/>
<pad name="P$14" x="4.12" y="3.92" drill="0.8128"/>
<pad name="P$15" x="4.12" y="6.46" drill="0.8128"/>
<pad name="P$16" x="4.12" y="9" drill="0.8128"/>
<wire x1="-4.5" y1="10" x2="-4.5" y2="-10" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="-10" x2="5" y2="-10" width="0.1524" layer="21"/>
<wire x1="5" y1="-10" x2="5" y2="10" width="0.1524" layer="21"/>
<wire x1="5" y1="10" x2="2" y2="10" width="0.1524" layer="21"/>
<wire x1="2" y1="10" x2="2" y2="8.5" width="0.1524" layer="21"/>
<wire x1="2" y1="8.5" x2="-1.5" y2="8.5" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="8.5" x2="-1.5" y2="10" width="0.1524" layer="21"/>
<wire x1="-1.5" y1="10" x2="-4.5" y2="10" width="0.1524" layer="21"/>
<text x="-4.5" y="11" size="1.778" layer="25">&gt;NAME</text>
</package>
<package name="DIL20" urn="urn:adsk.eagle:footprint:14349/1" locally_modified="yes">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="12.7" y1="2.921" x2="-12.7" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-2.921" x2="12.7" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="12.7" y1="2.921" x2="12.7" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="2.921" x2="-12.7" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-2.921" x2="-12.7" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.016" x2="-12.7" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-11.43" y="-3.81" drill="0.8128" rot="R90"/>
<pad name="2" x="-8.89" y="-3.81" drill="0.8128" rot="R90"/>
<pad name="7" x="3.81" y="-3.81" drill="0.8128" rot="R90"/>
<pad name="8" x="6.35" y="-3.81" drill="0.8128" rot="R90"/>
<pad name="3" x="-6.35" y="-3.81" drill="0.8128" rot="R90"/>
<pad name="4" x="-3.81" y="-3.81" drill="0.8128" rot="R90"/>
<pad name="6" x="1.27" y="-3.81" drill="0.8128" rot="R90"/>
<pad name="5" x="-1.27" y="-3.81" drill="0.8128" rot="R90"/>
<pad name="9" x="8.89" y="-3.81" drill="0.8128" rot="R90"/>
<pad name="10" x="11.43" y="-3.81" drill="0.8128" rot="R90"/>
<pad name="11" x="11.43" y="3.81" drill="0.8128" rot="R90"/>
<pad name="12" x="8.89" y="3.81" drill="0.8128" rot="R90"/>
<pad name="13" x="6.35" y="3.81" drill="0.8128" rot="R90"/>
<pad name="14" x="3.81" y="3.81" drill="0.8128" rot="R90"/>
<pad name="15" x="1.27" y="3.81" drill="0.8128" rot="R90"/>
<pad name="16" x="-1.27" y="3.81" drill="0.8128" rot="R90"/>
<pad name="17" x="-3.81" y="3.81" drill="0.8128" rot="R90"/>
<pad name="18" x="-6.35" y="3.81" drill="0.8128" rot="R90"/>
<pad name="19" x="-8.89" y="3.81" drill="0.8128" rot="R90"/>
<pad name="20" x="-11.43" y="3.81" drill="0.8128" rot="R90"/>
<text x="-13.081" y="-3.048" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="-9.779" y="-0.381" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="SO18W300" urn="urn:adsk.eagle:footprint:16304/1">
<description>&lt;b&gt;18-Lead Plastic Small Outline (SO) - Wide, 300 mil (SOIC)&lt;/b&gt;&lt;p&gt;
Source: http://www.rfsolutions.co.uk/acatalog/DS601-4.pdf</description>
<wire x1="5.85" y1="3.725" x2="-5.85" y2="3.725" width="0.1524" layer="21"/>
<wire x1="-5.85" y1="-3.725" x2="5.85" y2="-3.725" width="0.1524" layer="21"/>
<wire x1="5.85" y1="-3.725" x2="5.85" y2="3.725" width="0.1524" layer="21"/>
<wire x1="-5.85" y1="3.725" x2="-5.85" y2="-3.725" width="0.1524" layer="21"/>
<circle x="-4.5" y="-2.5" radius="0.5" width="0.1524" layer="21"/>
<smd name="1" x="-5.08" y="-4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="2" x="-3.81" y="-4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="3" x="-2.54" y="-4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="4" x="-1.27" y="-4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="5" x="0" y="-4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="6" x="1.27" y="-4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="7" x="2.54" y="-4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="8" x="3.81" y="-4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="11" x="3.81" y="4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="12" x="2.54" y="4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="13" x="1.27" y="4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="14" x="0" y="4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="15" x="-1.27" y="4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="16" x="-2.54" y="4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="17" x="-3.81" y="4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="18" x="-5.08" y="4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="10" x="5.08" y="4.9022" dx="0.6604" dy="2.032" layer="1"/>
<smd name="9" x="5.08" y="-4.9022" dx="0.6604" dy="2.032" layer="1"/>
<text x="-3.81" y="0" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.35" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-5.334" y1="-3.8608" x2="-4.826" y2="-3.7338" layer="21"/>
<rectangle x1="-5.334" y1="-5.334" x2="-4.826" y2="-3.8608" layer="51"/>
<rectangle x1="-4.064" y1="-3.8608" x2="-3.556" y2="-3.7338" layer="21"/>
<rectangle x1="-4.064" y1="-5.334" x2="-3.556" y2="-3.8608" layer="51"/>
<rectangle x1="-2.794" y1="-3.8608" x2="-2.286" y2="-3.7338" layer="21"/>
<rectangle x1="-2.794" y1="-5.334" x2="-2.286" y2="-3.8608" layer="51"/>
<rectangle x1="-1.524" y1="-3.8608" x2="-1.016" y2="-3.7338" layer="21"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-3.8608" layer="51"/>
<rectangle x1="-0.254" y1="-5.334" x2="0.254" y2="-3.8608" layer="51"/>
<rectangle x1="-0.254" y1="-3.8608" x2="0.254" y2="-3.7338" layer="21"/>
<rectangle x1="1.016" y1="-3.8608" x2="1.524" y2="-3.7338" layer="21"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-3.8608" layer="51"/>
<rectangle x1="2.286" y1="-3.8608" x2="2.794" y2="-3.7338" layer="21"/>
<rectangle x1="2.286" y1="-5.334" x2="2.794" y2="-3.8608" layer="51"/>
<rectangle x1="3.556" y1="-3.8608" x2="4.064" y2="-3.7338" layer="21"/>
<rectangle x1="3.556" y1="-5.334" x2="4.064" y2="-3.8608" layer="51"/>
<rectangle x1="-5.334" y1="3.8608" x2="-4.826" y2="5.334" layer="51"/>
<rectangle x1="-5.334" y1="3.7338" x2="-4.826" y2="3.8608" layer="21"/>
<rectangle x1="-4.064" y1="3.7338" x2="-3.556" y2="3.8608" layer="21"/>
<rectangle x1="-4.064" y1="3.8608" x2="-3.556" y2="5.334" layer="51"/>
<rectangle x1="-2.794" y1="3.7338" x2="-2.286" y2="3.8608" layer="21"/>
<rectangle x1="-2.794" y1="3.8608" x2="-2.286" y2="5.334" layer="51"/>
<rectangle x1="-1.524" y1="3.7338" x2="-1.016" y2="3.8608" layer="21"/>
<rectangle x1="-1.524" y1="3.8608" x2="-1.016" y2="5.334" layer="51"/>
<rectangle x1="-0.254" y1="3.7338" x2="0.254" y2="3.8608" layer="21"/>
<rectangle x1="-0.254" y1="3.8608" x2="0.254" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="3.7338" x2="1.524" y2="3.8608" layer="21"/>
<rectangle x1="1.016" y1="3.8608" x2="1.524" y2="5.334" layer="51"/>
<rectangle x1="2.286" y1="3.7338" x2="2.794" y2="3.8608" layer="21"/>
<rectangle x1="2.286" y1="3.8608" x2="2.794" y2="5.334" layer="51"/>
<rectangle x1="3.556" y1="3.7338" x2="4.064" y2="3.8608" layer="21"/>
<rectangle x1="3.556" y1="3.8608" x2="4.064" y2="5.334" layer="51"/>
<rectangle x1="4.826" y1="3.7338" x2="5.334" y2="3.8608" layer="21"/>
<rectangle x1="4.826" y1="-3.8608" x2="5.334" y2="-3.7338" layer="21"/>
<rectangle x1="4.826" y1="3.8608" x2="5.334" y2="5.334" layer="51"/>
<rectangle x1="4.826" y1="-5.334" x2="5.334" y2="-3.8608" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="MCP3008">
<pin name="CH0" x="-15.24" y="17.78" length="middle"/>
<pin name="CH1" x="-15.24" y="12.7" length="middle"/>
<pin name="CH2" x="-15.24" y="7.62" length="middle"/>
<pin name="CH3" x="-15.24" y="2.54" length="middle"/>
<pin name="CH4" x="-15.24" y="-2.54" length="middle"/>
<pin name="CH5" x="-15.24" y="-7.62" length="middle"/>
<pin name="CH6" x="-15.24" y="-12.7" length="middle"/>
<pin name="CH7" x="-15.24" y="-17.78" length="middle"/>
<pin name="DGND" x="15.24" y="-17.78" length="middle" rot="R180"/>
<pin name="CSS" x="15.24" y="-12.7" length="middle" rot="R180"/>
<pin name="MOSI" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="MISO" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="SCLK" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="AGND" x="15.24" y="7.62" length="middle" rot="R180"/>
<pin name="VREF" x="15.24" y="12.7" length="middle" rot="R180"/>
<pin name="VDD" x="15.24" y="17.78" length="middle" rot="R180"/>
<wire x1="-10.16" y1="20.32" x2="-10.16" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-20.32" x2="10.16" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="10.16" y1="-20.32" x2="10.16" y2="20.32" width="0.1524" layer="94"/>
<wire x1="10.16" y1="20.32" x2="-10.16" y2="20.32" width="0.1524" layer="94"/>
<text x="-10.16" y="22.86" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="SERVO12C">
<pin name="VDD" x="-12.7" y="12.7" length="middle"/>
<pin name="S0" x="-12.7" y="10.16" length="middle"/>
<pin name="S1" x="-12.7" y="7.62" length="middle"/>
<pin name="A2" x="-12.7" y="5.08" length="middle"/>
<pin name="A1" x="-12.7" y="2.54" length="middle"/>
<pin name="A0" x="-12.7" y="0" length="middle"/>
<pin name="S2" x="-12.7" y="-2.54" length="middle"/>
<pin name="S3" x="-12.7" y="-5.08" length="middle"/>
<pin name="S4" x="-12.7" y="-7.62" length="middle"/>
<pin name="S5" x="-12.7" y="-10.16" length="middle"/>
<pin name="SCL" x="12.7" y="-10.16" length="middle" rot="R180"/>
<pin name="S11" x="12.7" y="-7.62" length="middle" rot="R180"/>
<pin name="SDA" x="12.7" y="-5.08" length="middle" rot="R180"/>
<pin name="S10" x="12.7" y="-2.54" length="middle" rot="R180"/>
<pin name="S9" x="12.7" y="0" length="middle" rot="R180"/>
<pin name="S8" x="12.7" y="2.54" length="middle" rot="R180"/>
<pin name="NC" x="12.7" y="5.08" length="middle" rot="R180"/>
<pin name="S7" x="12.7" y="7.62" length="middle" rot="R180"/>
<pin name="S6" x="12.7" y="10.16" length="middle" rot="R180"/>
<pin name="VSS" x="12.7" y="12.7" length="middle" rot="R180"/>
<wire x1="-7.62" y1="15.24" x2="-7.62" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-12.7" x2="7.62" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="7.62" y2="15.24" width="0.1524" layer="94"/>
<wire x1="7.62" y1="15.24" x2="-7.62" y2="15.24" width="0.1524" layer="94"/>
<text x="-7.62" y="17.78" size="1.778" layer="95">&gt;NAME</text>
</symbol>
<symbol name="MCP23008">
<pin name="SCL" x="-10.16" y="12.7" length="middle"/>
<pin name="SDA" x="-10.16" y="10.16" length="middle"/>
<pin name="A2" x="-10.16" y="7.62" length="middle"/>
<pin name="A1" x="-10.16" y="5.08" length="middle"/>
<pin name="A0" x="-10.16" y="2.54" length="middle"/>
<pin name="RST" x="-10.16" y="0" length="middle"/>
<pin name="NC" x="-10.16" y="-2.54" length="middle"/>
<pin name="INT" x="-10.16" y="-5.08" length="middle"/>
<pin name="VSS" x="-10.16" y="-7.62" length="middle"/>
<pin name="IO0" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="IO1" x="15.24" y="-5.08" length="middle" rot="R180"/>
<pin name="IO2" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="IO3" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="IO4" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="IO5" x="15.24" y="5.08" length="middle" rot="R180"/>
<pin name="IO6" x="15.24" y="7.62" length="middle" rot="R180"/>
<wire x1="-5.08" y1="15.24" x2="-5.08" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-10.16" x2="10.16" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="10.16" y2="15.24" width="0.1524" layer="94"/>
<wire x1="10.16" y1="15.24" x2="-5.08" y2="15.24" width="0.1524" layer="94"/>
<text x="-5.08" y="17.78" size="1.778" layer="95">&gt;NAME</text>
<pin name="IO7" x="15.24" y="10.16" length="middle" rot="R180"/>
<pin name="VDD" x="15.24" y="12.7" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MCP3008">
<description>200Ksps  ADC</description>
<gates>
<gate name="G$1" symbol="MCP3008" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PDIP16">
<connects>
<connect gate="G$1" pin="AGND" pad="P$14"/>
<connect gate="G$1" pin="CH0" pad="P$1"/>
<connect gate="G$1" pin="CH1" pad="P$2"/>
<connect gate="G$1" pin="CH2" pad="P$3"/>
<connect gate="G$1" pin="CH3" pad="P$4"/>
<connect gate="G$1" pin="CH4" pad="P$5"/>
<connect gate="G$1" pin="CH5" pad="P$6"/>
<connect gate="G$1" pin="CH6" pad="P$7"/>
<connect gate="G$1" pin="CH7" pad="P$8"/>
<connect gate="G$1" pin="CSS" pad="P$10"/>
<connect gate="G$1" pin="DGND" pad="P$9"/>
<connect gate="G$1" pin="MISO" pad="P$12"/>
<connect gate="G$1" pin="MOSI" pad="P$11"/>
<connect gate="G$1" pin="SCLK" pad="P$13"/>
<connect gate="G$1" pin="VDD" pad="P$16"/>
<connect gate="G$1" pin="VREF" pad="P$15"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SERVO12C">
<gates>
<gate name="G$1" symbol="SERVO12C" x="0" y="-7.62"/>
</gates>
<devices>
<device name="" package="DIL20">
<connects>
<connect gate="G$1" pin="A0" pad="6"/>
<connect gate="G$1" pin="A1" pad="5"/>
<connect gate="G$1" pin="A2" pad="4"/>
<connect gate="G$1" pin="NC" pad="17"/>
<connect gate="G$1" pin="S0" pad="2"/>
<connect gate="G$1" pin="S1" pad="3"/>
<connect gate="G$1" pin="S10" pad="14"/>
<connect gate="G$1" pin="S11" pad="12"/>
<connect gate="G$1" pin="S2" pad="7"/>
<connect gate="G$1" pin="S3" pad="8"/>
<connect gate="G$1" pin="S4" pad="9"/>
<connect gate="G$1" pin="S5" pad="10"/>
<connect gate="G$1" pin="S6" pad="19"/>
<connect gate="G$1" pin="S7" pad="18"/>
<connect gate="G$1" pin="S8" pad="16"/>
<connect gate="G$1" pin="S9" pad="15"/>
<connect gate="G$1" pin="SCL" pad="11"/>
<connect gate="G$1" pin="SDA" pad="13"/>
<connect gate="G$1" pin="VDD" pad="1"/>
<connect gate="G$1" pin="VSS" pad="20"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MCP23008">
<description>I2C controlled GPIO expander</description>
<gates>
<gate name="G$1" symbol="MCP23008" x="-2.54" y="-2.54"/>
</gates>
<devices>
<device name="" package="SO18W300">
<connects>
<connect gate="G$1" pin="A0" pad="5"/>
<connect gate="G$1" pin="A1" pad="4"/>
<connect gate="G$1" pin="A2" pad="3"/>
<connect gate="G$1" pin="INT" pad="8"/>
<connect gate="G$1" pin="IO0" pad="10"/>
<connect gate="G$1" pin="IO1" pad="11"/>
<connect gate="G$1" pin="IO2" pad="12"/>
<connect gate="G$1" pin="IO3" pad="13"/>
<connect gate="G$1" pin="IO4" pad="14"/>
<connect gate="G$1" pin="IO5" pad="15"/>
<connect gate="G$1" pin="IO6" pad="16"/>
<connect gate="G$1" pin="IO7" pad="17"/>
<connect gate="G$1" pin="NC" pad="7"/>
<connect gate="G$1" pin="RST" pad="6"/>
<connect gate="G$1" pin="SCL" pad="1"/>
<connect gate="G$1" pin="SDA" pad="2"/>
<connect gate="G$1" pin="VDD" pad="18"/>
<connect gate="G$1" pin="VSS" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2" urn="urn:adsk.eagle:library:372">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26990/1" library_version="2">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VEE" urn="urn:adsk.eagle:symbol:26999/1" library_version="2">
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VEE" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:27037/1" prefix="SUPPLY" library_version="2">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VEE" urn="urn:adsk.eagle:component:27046/1" prefix="SUPPLY" library_version="2">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="VEE" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames" urn="urn:adsk.eagle:library:229">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC" urn="urn:adsk.eagle:symbol:13874/1" library_version="1">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" urn="urn:adsk.eagle:component:13926/1" prefix="FRAME" uservalue="yes" library_version="1">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="NANO_1" library="Arduino-clone" deviceset="NANO" device=""/>
<part name="NANO_2" library="Arduino-clone" deviceset="NANO" device=""/>
<part name="SUPPLY25" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY26" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY27" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="VEE" device=""/>
<part name="SUPPLY8" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY28" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY34" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="VEE" device=""/>
<part name="U6" library="rescue" deviceset="MCP3008" device=""/>
<part name="SUPPLY47" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY48" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="VEE" device=""/>
<part name="U$1" library="rescue" deviceset="SERVO12C" device=""/>
<part name="SUPPLY49" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY50" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="VEE" device=""/>
<part name="U$2" library="rescue" deviceset="MCP23008" device=""/>
<part name="SUPPLY52" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="GND" device=""/>
<part name="SUPPLY53" library="supply2" library_urn="urn:adsk.eagle:library:372" deviceset="VEE" device=""/>
<part name="FRAME1" library="frames" library_urn="urn:adsk.eagle:library:229" deviceset="A4L-LOC" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="NANO_1" gate="G$1" x="-38.1" y="59.69"/>
<instance part="NANO_2" gate="G$1" x="68.58" y="62.23"/>
<instance part="SUPPLY25" gate="GND" x="-5.08" y="41.91"/>
<instance part="SUPPLY26" gate="GND" x="-66.04" y="31.75"/>
<instance part="SUPPLY27" gate="G$1" x="-68.58" y="46.99"/>
<instance part="SUPPLY8" gate="GND" x="40.64" y="31.75"/>
<instance part="SUPPLY28" gate="GND" x="93.98" y="44.45"/>
<instance part="SUPPLY34" gate="G$1" x="38.1" y="46.99"/>
<instance part="U6" gate="G$1" x="64.77" y="-16.51"/>
<instance part="SUPPLY47" gate="GND" x="85.09" y="-39.37"/>
<instance part="SUPPLY48" gate="G$1" x="85.09" y="6.35"/>
<instance part="U$1" gate="G$1" x="-44.45" y="-10.16"/>
<instance part="SUPPLY49" gate="GND" x="-21.59" y="2.54" rot="R90"/>
<instance part="SUPPLY50" gate="G$1" x="-59.69" y="7.62"/>
<instance part="U$2" gate="G$1" x="-45.72" y="-48.26"/>
<instance part="SUPPLY52" gate="GND" x="-62.23" y="-60.96"/>
<instance part="SUPPLY53" gate="G$1" x="-29.21" y="-30.48"/>
<instance part="FRAME1" gate="G$1" x="-101.6" y="-80.01"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="NANO_1" gate="G$1" pin="GND2"/>
<wire x1="-27.94" y1="46.99" x2="-5.08" y2="46.99" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="46.99" x2="-5.08" y2="44.45" width="0.1524" layer="91"/>
<pinref part="SUPPLY25" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="NANO_1" gate="G$1" pin="GND"/>
<wire x1="-60.96" y1="41.91" x2="-66.04" y2="41.91" width="0.1524" layer="91"/>
<wire x1="-66.04" y1="41.91" x2="-66.04" y2="34.29" width="0.1524" layer="91"/>
<pinref part="SUPPLY26" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="NANO_2" gate="G$1" pin="GND"/>
<wire x1="45.72" y1="44.45" x2="40.64" y2="44.45" width="0.1524" layer="91"/>
<wire x1="40.64" y1="44.45" x2="40.64" y2="34.29" width="0.1524" layer="91"/>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="NANO_2" gate="G$1" pin="GND2"/>
<wire x1="78.74" y1="49.53" x2="93.98" y2="49.53" width="0.1524" layer="91"/>
<wire x1="93.98" y1="49.53" x2="93.98" y2="46.99" width="0.1524" layer="91"/>
<pinref part="SUPPLY28" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="AGND"/>
<wire x1="80.01" y1="-8.89" x2="85.09" y2="-8.89" width="0.1524" layer="91"/>
<label x="85.09" y="-8.89" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="DGND"/>
<wire x1="80.01" y1="-34.29" x2="85.09" y2="-34.29" width="0.1524" layer="91"/>
<wire x1="85.09" y1="-34.29" x2="85.09" y2="-36.83" width="0.1524" layer="91"/>
<pinref part="SUPPLY47" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="VSS"/>
<wire x1="-31.75" y1="2.54" x2="-24.13" y2="2.54" width="0.1524" layer="91"/>
<pinref part="SUPPLY49" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="A2"/>
<wire x1="-57.15" y1="-5.08" x2="-59.69" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="-59.69" y1="-5.08" x2="-59.69" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="A1"/>
<wire x1="-59.69" y1="-7.62" x2="-57.15" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="A0"/>
<wire x1="-57.15" y1="-10.16" x2="-59.69" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="-59.69" y1="-10.16" x2="-59.69" y2="-7.62" width="0.1524" layer="91"/>
<junction x="-59.69" y="-7.62"/>
<wire x1="-59.69" y1="-7.62" x2="-64.77" y2="-7.62" width="0.1524" layer="91"/>
<label x="-64.77" y="-7.62" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="VSS"/>
<wire x1="-55.88" y1="-55.88" x2="-62.23" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="-62.23" y1="-55.88" x2="-62.23" y2="-58.42" width="0.1524" layer="91"/>
<pinref part="SUPPLY52" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="A2"/>
<wire x1="-55.88" y1="-40.64" x2="-58.42" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="-40.64" x2="-58.42" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="A1"/>
<wire x1="-58.42" y1="-43.18" x2="-55.88" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="A0"/>
<wire x1="-55.88" y1="-45.72" x2="-58.42" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="-45.72" x2="-58.42" y2="-43.18" width="0.1524" layer="91"/>
<junction x="-58.42" y="-43.18"/>
<wire x1="-58.42" y1="-43.18" x2="-60.96" y2="-43.18" width="0.1524" layer="91"/>
<label x="-60.96" y="-43.18" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SERVO_Y" class="0">
<segment>
<pinref part="NANO_1" gate="G$1" pin="D5"/>
<wire x1="-27.94" y1="57.15" x2="-25.4" y2="57.15" width="0.1524" layer="91"/>
<label x="-25.4" y="57.15" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="S8"/>
<wire x1="-31.75" y1="-7.62" x2="-29.21" y2="-7.62" width="0.1524" layer="91"/>
<label x="-29.21" y="-7.62" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SERVO_X" class="0">
<segment>
<pinref part="NANO_1" gate="G$1" pin="D3"/>
<wire x1="-27.94" y1="52.07" x2="-25.4" y2="52.07" width="0.1524" layer="91"/>
<label x="-25.4" y="52.07" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="S9"/>
<wire x1="-31.75" y1="-10.16" x2="-29.21" y2="-10.16" width="0.1524" layer="91"/>
<label x="-29.21" y="-10.16" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="VEE" class="0">
<segment>
<pinref part="NANO_1" gate="G$1" pin="RAW"/>
<pinref part="SUPPLY27" gate="G$1" pin="VEE"/>
<wire x1="-60.96" y1="39.37" x2="-68.58" y2="39.37" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="39.37" x2="-68.58" y2="44.45" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="NANO_2" gate="G$1" pin="RAW"/>
<wire x1="45.72" y1="41.91" x2="38.1" y2="41.91" width="0.1524" layer="91"/>
<wire x1="38.1" y1="41.91" x2="38.1" y2="44.45" width="0.1524" layer="91"/>
<pinref part="SUPPLY34" gate="G$1" pin="VEE"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="VREF"/>
<wire x1="80.01" y1="-3.81" x2="85.09" y2="-3.81" width="0.1524" layer="91"/>
<pinref part="U6" gate="G$1" pin="VDD"/>
<wire x1="85.09" y1="-3.81" x2="85.09" y2="1.27" width="0.1524" layer="91"/>
<wire x1="85.09" y1="1.27" x2="80.01" y2="1.27" width="0.1524" layer="91"/>
<wire x1="85.09" y1="1.27" x2="85.09" y2="3.81" width="0.1524" layer="91"/>
<junction x="85.09" y="1.27"/>
<pinref part="SUPPLY48" gate="G$1" pin="VEE"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="VDD"/>
<wire x1="-57.15" y1="2.54" x2="-59.69" y2="2.54" width="0.1524" layer="91"/>
<wire x1="-59.69" y1="2.54" x2="-59.69" y2="5.08" width="0.1524" layer="91"/>
<pinref part="SUPPLY50" gate="G$1" pin="VEE"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="VDD"/>
<wire x1="-30.48" y1="-35.56" x2="-29.21" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-29.21" y1="-35.56" x2="-29.21" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="SUPPLY53" gate="G$1" pin="VEE"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="RST"/>
<wire x1="-55.88" y1="-48.26" x2="-62.23" y2="-48.26" width="0.1524" layer="91"/>
<label x="-62.23" y="-48.26" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="A_TX" class="0">
<segment>
<pinref part="NANO_2" gate="G$1" pin="RX1"/>
<wire x1="81.28" y1="44.45" x2="78.74" y2="44.45" width="0.1524" layer="91"/>
<label x="81.28" y="44.45" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="A_RX" class="0">
<segment>
<pinref part="NANO_2" gate="G$1" pin="TX0"/>
<wire x1="81.28" y1="41.91" x2="78.74" y2="41.91" width="0.1524" layer="91"/>
<label x="81.28" y="41.91" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="A_MOSI" class="0">
<segment>
<pinref part="NANO_2" gate="G$1" pin="D11"/>
<wire x1="78.74" y1="74.93" x2="81.28" y2="74.93" width="0.1524" layer="91"/>
<label x="81.28" y="74.93" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="NANO_1" gate="G$1" pin="D11"/>
<wire x1="-27.94" y1="72.39" x2="-25.4" y2="72.39" width="0.1524" layer="91"/>
<label x="-25.4" y="72.39" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="MOSI"/>
<wire x1="80.01" y1="-24.13" x2="87.63" y2="-24.13" width="0.1524" layer="91"/>
<label x="87.63" y="-24.13" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="A_MISO" class="0">
<segment>
<pinref part="NANO_2" gate="G$1" pin="D12"/>
<wire x1="78.74" y1="77.47" x2="81.28" y2="77.47" width="0.1524" layer="91"/>
<label x="81.28" y="77.47" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="NANO_1" gate="G$1" pin="D12"/>
<wire x1="-27.94" y1="74.93" x2="-25.4" y2="74.93" width="0.1524" layer="91"/>
<label x="-25.4" y="74.93" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="MISO"/>
<wire x1="80.01" y1="-19.05" x2="87.63" y2="-19.05" width="0.1524" layer="91"/>
<label x="87.63" y="-19.05" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="A_SPI_CLK" class="0">
<segment>
<pinref part="NANO_2" gate="G$1" pin="D13"/>
<wire x1="45.72" y1="77.47" x2="43.18" y2="77.47" width="0.1524" layer="91"/>
<label x="43.18" y="77.47" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="NANO_1" gate="G$1" pin="D13"/>
<wire x1="-60.96" y1="74.93" x2="-63.5" y2="74.93" width="0.1524" layer="91"/>
<label x="-63.5" y="74.93" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="SCLK"/>
<wire x1="80.01" y1="-13.97" x2="87.63" y2="-13.97" width="0.1524" layer="91"/>
<label x="87.63" y="-13.97" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="EN1" class="0">
<segment>
<pinref part="NANO_1" gate="G$1" pin="D8"/>
<wire x1="-27.94" y1="64.77" x2="-25.4" y2="64.77" width="0.1524" layer="91"/>
<label x="-25.4" y="64.77" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="IO7"/>
<wire x1="-30.48" y1="-38.1" x2="-26.67" y2="-38.1" width="0.1524" layer="91"/>
<label x="-26.67" y="-38.1" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="EN2" class="0">
<segment>
<pinref part="NANO_1" gate="G$1" pin="D7"/>
<wire x1="-27.94" y1="62.23" x2="-25.4" y2="62.23" width="0.1524" layer="91"/>
<label x="-25.4" y="62.23" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="IO6"/>
<wire x1="-30.48" y1="-40.64" x2="-26.67" y2="-40.64" width="0.1524" layer="91"/>
<label x="-26.67" y="-40.64" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="EN3" class="0">
<segment>
<pinref part="NANO_1" gate="G$1" pin="D4"/>
<wire x1="-27.94" y1="54.61" x2="-25.4" y2="54.61" width="0.1524" layer="91"/>
<label x="-25.4" y="54.61" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="IO5"/>
<wire x1="-30.48" y1="-43.18" x2="-26.67" y2="-43.18" width="0.1524" layer="91"/>
<label x="-26.67" y="-43.18" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="EN4" class="0">
<segment>
<pinref part="NANO_1" gate="G$1" pin="D2"/>
<wire x1="-27.94" y1="49.53" x2="-25.4" y2="49.53" width="0.1524" layer="91"/>
<label x="-25.4" y="49.53" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="IO4"/>
<wire x1="-30.48" y1="-45.72" x2="-26.67" y2="-45.72" width="0.1524" layer="91"/>
<label x="-26.67" y="-45.72" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SPEED_B" class="0">
<segment>
<pinref part="NANO_1" gate="G$1" pin="D6"/>
<wire x1="-27.94" y1="59.69" x2="-25.4" y2="59.69" width="0.1524" layer="91"/>
<label x="-25.4" y="59.69" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="S7"/>
<wire x1="-31.75" y1="-2.54" x2="-29.21" y2="-2.54" width="0.1524" layer="91"/>
<label x="-29.21" y="-2.54" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SPEED_A" class="0">
<segment>
<pinref part="NANO_1" gate="G$1" pin="D9"/>
<wire x1="-27.94" y1="67.31" x2="-25.4" y2="67.31" width="0.1524" layer="91"/>
<label x="-25.4" y="67.31" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="S6"/>
<wire x1="-31.75" y1="0" x2="-29.21" y2="0" width="0.1524" layer="91"/>
<label x="-29.21" y="0" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="A_CS0" class="0">
<segment>
<pinref part="NANO_2" gate="G$1" pin="D10"/>
<wire x1="78.74" y1="72.39" x2="81.28" y2="72.39" width="0.1524" layer="91"/>
<label x="81.28" y="72.39" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="CSS"/>
<wire x1="80.01" y1="-29.21" x2="87.63" y2="-29.21" width="0.1524" layer="91"/>
<label x="87.63" y="-29.21" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="M1" class="0">
<segment>
<pinref part="NANO_2" gate="G$1" pin="A0"/>
<wire x1="45.72" y1="69.85" x2="40.64" y2="69.85" width="0.1524" layer="91"/>
<label x="40.64" y="69.85" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="CH3"/>
<wire x1="49.53" y1="-13.97" x2="41.91" y2="-13.97" width="0.1524" layer="91"/>
<label x="41.91" y="-13.97" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="M2" class="0">
<segment>
<pinref part="NANO_2" gate="G$1" pin="A1"/>
<wire x1="45.72" y1="67.31" x2="40.64" y2="67.31" width="0.1524" layer="91"/>
<label x="40.64" y="67.31" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="CH4"/>
<wire x1="49.53" y1="-19.05" x2="41.91" y2="-19.05" width="0.1524" layer="91"/>
<label x="41.91" y="-19.05" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="M3" class="0">
<segment>
<pinref part="NANO_2" gate="G$1" pin="A2"/>
<wire x1="45.72" y1="64.77" x2="40.64" y2="64.77" width="0.1524" layer="91"/>
<label x="40.64" y="64.77" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U6" gate="G$1" pin="CH5"/>
<wire x1="49.53" y1="-24.13" x2="41.91" y2="-24.13" width="0.1524" layer="91"/>
<label x="41.91" y="-24.13" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="TRIG" class="0">
<segment>
<pinref part="NANO_1" gate="G$1" pin="RX1"/>
<wire x1="-27.94" y1="41.91" x2="-25.4" y2="41.91" width="0.1524" layer="91"/>
<label x="-25.4" y="41.91" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="IO3"/>
<wire x1="-30.48" y1="-48.26" x2="-26.67" y2="-48.26" width="0.1524" layer="91"/>
<label x="-26.67" y="-48.26" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="ECHO" class="0">
<segment>
<pinref part="NANO_1" gate="G$1" pin="TX0"/>
<wire x1="-27.94" y1="39.37" x2="-25.4" y2="39.37" width="0.1524" layer="91"/>
<label x="-25.4" y="39.37" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="IO2"/>
<wire x1="-30.48" y1="-50.8" x2="-26.67" y2="-50.8" width="0.1524" layer="91"/>
<label x="-26.67" y="-50.8" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="A_CS1" class="0">
<segment>
<pinref part="NANO_1" gate="G$1" pin="D10"/>
<wire x1="-27.94" y1="69.85" x2="-25.4" y2="69.85" width="0.1524" layer="91"/>
<label x="-25.4" y="69.85" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="NANO_2" gate="G$1" pin="A5"/>
<wire x1="45.72" y1="57.15" x2="17.78" y2="57.15" width="0.1524" layer="91"/>
<label x="17.78" y="57.15" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="NANO_1" gate="G$1" pin="A5"/>
<wire x1="-60.96" y1="54.61" x2="-86.36" y2="54.61" width="0.1524" layer="91"/>
<label x="-86.36" y="54.61" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="SCL"/>
<wire x1="-31.75" y1="-20.32" x2="-26.67" y2="-20.32" width="0.1524" layer="91"/>
<label x="-26.67" y="-20.32" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="SCL"/>
<wire x1="-55.88" y1="-35.56" x2="-62.23" y2="-35.56" width="0.1524" layer="91"/>
<label x="-62.23" y="-35.56" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="NANO_2" gate="G$1" pin="A4"/>
<wire x1="45.72" y1="59.69" x2="27.94" y2="59.69" width="0.1524" layer="91"/>
<label x="27.94" y="59.69" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="NANO_1" gate="G$1" pin="A4"/>
<wire x1="-60.96" y1="57.15" x2="-78.74" y2="57.15" width="0.1524" layer="91"/>
<label x="-78.74" y="57.15" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="SDA"/>
<wire x1="-31.75" y1="-15.24" x2="-26.67" y2="-15.24" width="0.1524" layer="91"/>
<label x="-26.67" y="-15.24" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="SDA"/>
<wire x1="-55.88" y1="-38.1" x2="-62.23" y2="-38.1" width="0.1524" layer="91"/>
<label x="-62.23" y="-38.1" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="A0" class="0">
<segment>
<pinref part="NANO_1" gate="G$1" pin="A0"/>
<wire x1="-60.96" y1="67.31" x2="-66.04" y2="67.31" width="0.1524" layer="91"/>
<label x="-66.04" y="67.31" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="A1" class="0">
<segment>
<pinref part="NANO_1" gate="G$1" pin="A1"/>
<wire x1="-60.96" y1="64.77" x2="-66.04" y2="64.77" width="0.1524" layer="91"/>
<label x="-66.04" y="64.77" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="D0" class="0">
<segment>
<pinref part="NANO_2" gate="G$1" pin="D9"/>
<wire x1="78.74" y1="69.85" x2="81.28" y2="69.85" width="0.1524" layer="91"/>
<label x="81.28" y="69.85" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="D1" class="0">
<segment>
<pinref part="NANO_2" gate="G$1" pin="D8"/>
<wire x1="78.74" y1="67.31" x2="81.28" y2="67.31" width="0.1524" layer="91"/>
<label x="81.28" y="67.31" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
