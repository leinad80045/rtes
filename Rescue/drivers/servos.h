#include <wiringPiI2C.h>
#include <wiringPi.h>
#include <stdio.h>

class servos
{
	public:
		servos();
		void yUp();
		void yDown();
		void xRight();
		void xLeft();
		
	private:
		int deviceAddress;
		int x; 
		int y;
};
	

