#include <wiringPiI2C.h>
#include <wiringPi.h>
#include <stdio.h>
#include "servos.h"

#define yController 8
#define xController 9


servos::servos(){
	
	//Set the I2C divice address 
	deviceAddress = wiringPiI2CSetup(0x28);
	
	//Setting up the servo controller extended mode 
	wiringPiI2CWriteReg8(deviceAddress, 20, 0);
	
	//Setting both servos in the middle 
	x = 127;
	y = 127;
	wiringPiI2CWriteReg8(deviceAddress, yController, y);
	wiringPiI2CWriteReg8(deviceAddress, xController, x);
	
}

void servos::yUp(){
	
	y += 1;
	
	if(y >= 255)
		y = 255;
	
	wiringPiI2CWriteReg8(deviceAddress, yController, y);
}

void servos::yDown(){
	
	y -= 1;
	
	if(y <= 0)
		y = 0;
	
	wiringPiI2CWriteReg8(deviceAddress, yController, y);
}

void servos::xRight(){
	
	x += 1;
	
	if(x >= 255)
		x =255;
	
	wiringPiI2CWriteReg8(deviceAddress, xController, x);
}

void servos::xLeft(){
	
	x -= 1;
	
	if(x <= 0)
		x = 0;
	
	wiringPiI2CWriteReg8(deviceAddress, xController, x);	
}
	



