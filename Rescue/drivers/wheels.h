#include <wiringPiI2C.h>
#include <wiringPi.h>
#include <stdio.h>

class wheels{

	public:
	
		wheels();
		void moveTowards();
		void moveBackwards();
		void turnLeft();
		void turnRight();
		void stop();
		
	private:
	
		int deviceAddress;
		
};
