#include <wiringPiI2C.h>
#include <wiringPi.h>
#include <stdio.h>
#include "wheels.h"

#define outputs 0x0F
#define stio 0x00
#define gpio 0x09
#define towards 0xA0
#define backwards 0x50
#define left 0x90
#define right 0x60


wheels::wheels(){
	
	deviceAddress = wiringPiI2CSetup(0x20); //device address
	wiringPiI2CWriteReg8(deviceAddress, stio, outputs); //setting device outputs
}

void wheels::moveTowards(){
	
	wiringPiI2CWriteReg8(deviceAddress, gpio, towards); 
}

void wheels::moveBackwards(){
	
	wiringPiI2CWriteReg8(deviceAddress, gpio, backwards); 
}

void wheels::turnLeft(){
	
	wiringPiI2CWriteReg8(deviceAddress, gpio, left); 
}

void wheels::turnRight(){
	
	wiringPiI2CWriteReg8(deviceAddress, gpio, right); 
}

void wheels::stop(){
	
	wiringPiI2CWriteReg8(deviceAddress, gpio, 0x00);
}
	


	
	
	
	
