#include <wiringPiI2C.h>
#include <wiringPi.h>
#include <stdio.h>
#include "wheels.h"

#define outputs 0x0F
#define stio 0x00
#define gpio 0x09
#define towards 0x90  
#define backwards 0x60 
#define left 0x50 
#define right 0xA0 


void wheels(){
	
	deviceAddress = wiringPiI2CSetup(0x20); //device address
	wiringPiI2CWriteReg8(deviceAddress, stio, outputs); //setting device outputs
}

void moveTowards(){
	
	wiringPiI2CWriteReg8(deviceAddress, gpio, towards); 
}

void moveBackwards(){
	
	wiringPiI2CWriteReg8(deviceAddress, gpio, backwards); 
}

void turnLeft(){
	
	wiringPiI2CWriteReg8(deviceAddress, gpio, left); 
}

void turnRight(){
	
	wiringPiI2CWriteReg8(deviceAddress, gpio, right); 
}

void stop(){
	
	wiringPiI2CWriteReg8(deviceAddress, gpio, 0x00);
}
	


	
	
	
	
