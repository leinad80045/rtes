#include <wiringPiI2C.h>
#include <wiringPi.h>
#include <stdio.h>
#include "servos.h"

#define yController 8
#define xController 9


void servos(){
	
	//Set the I2C divice address 
	servoMotor = wiringPiI2CSetup(0x28);
	
	//Setting up the servo controller extended mode 
	wiringPiI2CWriteReg8(servoMotor, 20, 0);
	
	//Setting both servos in the middle 
	x = 127;
	y = 127;
	wiringPiI2CWriteReg8(servoMotor, yController, y);
	wiringPiI2CWriteReg8(servoMotor, xController, x);
	
}

void yUp(){
	
	y -= 5;
	
	if(y <= 0)
		y = 0;
	
	wiringPiI2CWriteReg8(servoMotor, yController, y);
}

void yDown(){
	
	y += 5;
	
	if(y >= 255)
		y = 255;
	
	wiringPiI2CWriteReg8(servoMotor, yController, y);
}

void xRight(){
	
	x -= 5;
	
	if(x <= 0)
		x = 0;
	
	wiringPiI2CWriteReg8(servoMotor, xController, x);
}

void xLeft(){
	
	x += 5;
	
	if(x >= 255)
		x = 255;
	
	wiringPiI2CWriteReg8(servoMotor, xController, x);	
}
	



