var structrotdata =
[
    [ "axis", "structrotdata.html#ac4c0046abfda10e4f449faa3db4c7588", null ],
    [ "buffer_high", "structrotdata.html#a8ce772cded3854b6548bb9c77c0bea08", null ],
    [ "buffer_norm", "structrotdata.html#ac7a0e666ca08ec07468ad08affa4f104", null ],
    [ "capture_height_high", "structrotdata.html#a24f91e3e52342fea9abc9d108a3ea4b0", null ],
    [ "capture_height_norm", "structrotdata.html#aa66e492e18710de5032c0b6a5a9b8844", null ],
    [ "capture_width_high", "structrotdata.html#aaaba5686b6a1638b235d58697f50d030", null ],
    [ "capture_width_norm", "structrotdata.html#a96bf6ebac53351b108fb3f2348d3f42b", null ],
    [ "degrees", "structrotdata.html#ac057fa5bdf67ece3a224093cc68590af", null ]
];