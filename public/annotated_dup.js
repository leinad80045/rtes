var annotated_dup =
[
    [ "androidx", null, [
      [ "appcompat", null, [
        [ "R", "classandroidx_1_1appcompat_1_1_r.html", null ]
      ] ],
      [ "arch", null, [
        [ "core", null, [
          [ "R", "classandroidx_1_1arch_1_1core_1_1_r.html", null ]
        ] ]
      ] ],
      [ "asynclayoutinflater", null, [
        [ "R", "classandroidx_1_1asynclayoutinflater_1_1_r.html", null ]
      ] ],
      [ "constraintlayout", null, [
        [ "widget", null, [
          [ "R", "classandroidx_1_1constraintlayout_1_1widget_1_1_r.html", null ]
        ] ]
      ] ],
      [ "coordinatorlayout", null, [
        [ "R", "classandroidx_1_1coordinatorlayout_1_1_r.html", null ]
      ] ],
      [ "core", null, [
        [ "R", "classandroidx_1_1core_1_1_r.html", null ]
      ] ],
      [ "cursoradapter", null, [
        [ "R", "classandroidx_1_1cursoradapter_1_1_r.html", null ]
      ] ],
      [ "customview", null, [
        [ "R", "classandroidx_1_1customview_1_1_r.html", null ]
      ] ],
      [ "documentfile", null, [
        [ "R", "classandroidx_1_1documentfile_1_1_r.html", null ]
      ] ],
      [ "drawerlayout", null, [
        [ "R", "classandroidx_1_1drawerlayout_1_1_r.html", null ]
      ] ],
      [ "fragment", null, [
        [ "R", "classandroidx_1_1fragment_1_1_r.html", null ]
      ] ],
      [ "interpolator", null, [
        [ "R", "classandroidx_1_1interpolator_1_1_r.html", null ]
      ] ],
      [ "legacy", null, [
        [ "coreui", null, [
          [ "R", "classandroidx_1_1legacy_1_1coreui_1_1_r.html", null ]
        ] ],
        [ "coreutils", null, [
          [ "R", "classandroidx_1_1legacy_1_1coreutils_1_1_r.html", null ]
        ] ]
      ] ],
      [ "lifecycle", null, [
        [ "livedata", null, [
          [ "core", null, [
            [ "R", "classandroidx_1_1lifecycle_1_1livedata_1_1core_1_1_r.html", null ]
          ] ],
          [ "R", "classandroidx_1_1lifecycle_1_1livedata_1_1_r.html", null ]
        ] ],
        [ "viewmodel", null, [
          [ "R", "classandroidx_1_1lifecycle_1_1viewmodel_1_1_r.html", null ]
        ] ],
        [ "R", "classandroidx_1_1lifecycle_1_1_r.html", null ]
      ] ],
      [ "loader", null, [
        [ "R", "classandroidx_1_1loader_1_1_r.html", null ]
      ] ],
      [ "localbroadcastmanager", null, [
        [ "R", "classandroidx_1_1localbroadcastmanager_1_1_r.html", null ]
      ] ],
      [ "print", null, [
        [ "R", "classandroidx_1_1print_1_1_r.html", null ]
      ] ],
      [ "slidingpanelayout", null, [
        [ "R", "classandroidx_1_1slidingpanelayout_1_1_r.html", null ]
      ] ],
      [ "swiperefreshlayout", null, [
        [ "R", "classandroidx_1_1swiperefreshlayout_1_1_r.html", null ]
      ] ],
      [ "vectordrawable", null, [
        [ "R", "classandroidx_1_1vectordrawable_1_1_r.html", null ]
      ] ],
      [ "versionedparcelable", null, [
        [ "R", "classandroidx_1_1versionedparcelable_1_1_r.html", null ]
      ] ],
      [ "viewpager", null, [
        [ "R", "classandroidx_1_1viewpager_1_1_r.html", null ]
      ] ]
    ] ],
    [ "com", null, [
      [ "example", null, [
        [ "rescu_e_interface", "namespacecom_1_1example_1_1rescu__e__interface.html", "namespacecom_1_1example_1_1rescu__e__interface" ],
        [ "servocontrollerwifi", "namespacecom_1_1example_1_1servocontrollerwifi.html", "namespacecom_1_1example_1_1servocontrollerwifi" ]
      ] ]
    ] ],
    [ "auth_param", "structauth__param.html", "structauth__param" ],
    [ "code_table_t", "structcode__table__t.html", "structcode__table__t" ],
    [ "COMMAND_LIST", "struct_c_o_m_m_a_n_d___l_i_s_t.html", "struct_c_o_m_m_a_n_d___l_i_s_t" ],
    [ "config", "structconfig.html", "structconfig" ],
    [ "config_param", "structconfig__param.html", "structconfig__param" ],
    [ "context", "structcontext.html", "structcontext" ],
    [ "coord", "structcoord.html", "structcoord" ],
    [ "dep_config_param", "structdep__config__param.html", "structdep__config__param" ],
    [ "draw_char", "structdraw__char.html", "structdraw__char" ],
    [ "event_handlers", "structevent__handlers.html", "structevent__handlers" ],
    [ "ffmpeg", "structffmpeg.html", "structffmpeg" ],
    [ "file_context", "structfile__context.html", "structfile__context" ],
    [ "ftp_context", "structftp__context.html", "structftp__context" ],
    [ "image_data", "structimage__data.html", "structimage__data" ],
    [ "images", "structimages.html", "structimages" ],
    [ "imgsize_context", "structimgsize__context.html", "structimgsize__context" ],
    [ "jpgutl_error_mgr", "structjpgutl__error__mgr.html", "structjpgutl__error__mgr" ],
    [ "MD5_CTX", "struct_m_d5___c_t_x.html", "struct_m_d5___c_t_x" ],
    [ "mem_destination_mgr", "structmem__destination__mgr.html", "structmem__destination__mgr" ],
    [ "mhdstart_ctx", "structmhdstart__ctx.html", "structmhdstart__ctx" ],
    [ "mjpg_header", "structmjpg__header.html", "structmjpg__header" ],
    [ "mmal_param_colourfx_s", "structmmal__param__colourfx__s.html", "structmmal__param__colourfx__s" ],
    [ "mmal_param_thumbnail_config_s", "structmmal__param__thumbnail__config__s.html", "structmmal__param__thumbnail__config__s" ],
    [ "mmalcam_context", "structmmalcam__context.html", "structmmalcam__context" ],
    [ "mmx_t", "unionmmx__t.html", "unionmmx__t" ],
    [ "netcam_caps", "structnetcam__caps.html", "structnetcam__caps" ],
    [ "netcam_context", "structnetcam__context.html", "structnetcam__context" ],
    [ "netcam_image_buff", "structnetcam__image__buff.html", "structnetcam__image__buff" ],
    [ "netcam_source_mgr", "structnetcam__source__mgr.html", "structnetcam__source__mgr" ],
    [ "param_float_rect_s", "structparam__float__rect__s.html", "structparam__float__rect__s" ],
    [ "pwc_coord", "structpwc__coord.html", "structpwc__coord" ],
    [ "pwc_imagesize", "structpwc__imagesize.html", "structpwc__imagesize" ],
    [ "pwc_leds", "structpwc__leds.html", "structpwc__leds" ],
    [ "pwc_mpt_angles", "structpwc__mpt__angles.html", "structpwc__mpt__angles" ],
    [ "pwc_mpt_range", "structpwc__mpt__range.html", "structpwc__mpt__range" ],
    [ "pwc_mpt_status", "structpwc__mpt__status.html", "structpwc__mpt__status" ],
    [ "pwc_probe", "structpwc__probe.html", "structpwc__probe" ],
    [ "pwc_serial", "structpwc__serial.html", "structpwc__serial" ],
    [ "pwc_table_init_buffer", "structpwc__table__init__buffer.html", "structpwc__table__init__buffer" ],
    [ "pwc_video_command", "structpwc__video__command.html", "structpwc__video__command" ],
    [ "pwc_wb_speed", "structpwc__wb__speed.html", "structpwc__wb__speed" ],
    [ "pwc_whitebalance", "structpwc__whitebalance.html", "structpwc__whitebalance" ],
    [ "raspicam_camera_parameters_s", "structraspicam__camera__parameters__s.html", "structraspicam__camera__parameters__s" ],
    [ "rbuf", "structrbuf.html", "structrbuf" ],
    [ "rotdata", "structrotdata.html", "structrotdata" ],
    [ "rtsp_context", "structrtsp__context.html", "structrtsp__context" ],
    [ "segment", "structsegment.html", "structsegment" ],
    [ "Segment", "struct_segment.html", "struct_segment" ],
    [ "servos", "classservos.html", "classservos" ],
    [ "stream", "structstream.html", "structstream" ],
    [ "stream_buffer", "structstream__buffer.html", "structstream__buffer" ],
    [ "stream_data", "structstream__data.html", "structstream__data" ],
    [ "strminfo_ctx", "structstrminfo__ctx.html", "structstrminfo__ctx" ],
    [ "tiff_writing", "structtiff__writing.html", "structtiff__writing" ],
    [ "trackoptions", "structtrackoptions.html", "structtrackoptions" ],
    [ "url_t", "structurl__t.html", "structurl__t" ],
    [ "vdev_context", "structvdev__context.html", "structvdev__context" ],
    [ "vdev_usrctrl_ctx", "structvdev__usrctrl__ctx.html", "structvdev__usrctrl__ctx" ],
    [ "vid_devctrl_ctx", "structvid__devctrl__ctx.html", "structvid__devctrl__ctx" ],
    [ "video_dev", "structvideo__dev.html", "structvideo__dev" ],
    [ "webui_ctx", "structwebui__ctx.html", "structwebui__ctx" ],
    [ "wheels", "classwheels.html", "classwheels" ],
    [ "xref_t", "structxref__t.html", "structxref__t" ]
];