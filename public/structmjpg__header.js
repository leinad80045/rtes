var structmjpg__header =
[
    [ "mh_chunksize", "structmjpg__header.html#a954ca2a29d95826f6cc777f2630a7371", null ],
    [ "mh_frameheight", "structmjpg__header.html#a877933f1375c6da47523c5eabde77868", null ],
    [ "mh_frameoffset", "structmjpg__header.html#a161d2eca9556939ebabfb4b95b853b9f", null ],
    [ "mh_framesize", "structmjpg__header.html#a5cdfb023287dd072218000f0b6a2b314", null ],
    [ "mh_framewidth", "structmjpg__header.html#a43a97dfecae42f4d5d6804817d2fb7ba", null ],
    [ "mh_magic", "structmjpg__header.html#a6336ff4473091e2cbc2ca008bbd6b7aa", null ],
    [ "mh_reserved", "structmjpg__header.html#a97d5d90cf256d97e61cb5cc6ef1975f2", null ]
];