var structpwc__video__command =
[
    [ "alternate", "structpwc__video__command.html#ad634dea05196a70227cf5131429a3754", null ],
    [ "bandlength", "structpwc__video__command.html#ac6d42868f04fe9a0555c1eaeadc57701", null ],
    [ "command_buf", "structpwc__video__command.html#aa90eba3a68e8e651ac6238015e4087ca", null ],
    [ "command_len", "structpwc__video__command.html#accce19fc4cf338f3f14f93ff28279305", null ],
    [ "frame_size", "structpwc__video__command.html#a9480a6ee5900c5cd6740c61a51ae289e", null ],
    [ "release", "structpwc__video__command.html#ad62700ff8d197f660139f5fb73f2ae4c", null ],
    [ "size", "structpwc__video__command.html#a523f82f30a0b98324633d6925b2d0025", null ],
    [ "type", "structpwc__video__command.html#aced3a1a4dfb38e52b6f14d7fe671ce97", null ]
];