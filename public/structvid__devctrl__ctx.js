var structvid__devctrl__ctx =
[
    [ "ctrl_currval", "structvid__devctrl__ctx.html#aedd16c66d10acf4fc13690b33dab4e95", null ],
    [ "ctrl_default", "structvid__devctrl__ctx.html#a0de41871b471c50487bed09a9450dd52", null ],
    [ "ctrl_id", "structvid__devctrl__ctx.html#ab4463e3dda2f1446c73fbed1e1f49db4", null ],
    [ "ctrl_iddesc", "structvid__devctrl__ctx.html#a1e234b546f57839ffaf7f29e1ba02846", null ],
    [ "ctrl_maximum", "structvid__devctrl__ctx.html#afa1367bd23b99a23e454b09e3c79bfd0", null ],
    [ "ctrl_menuitem", "structvid__devctrl__ctx.html#a42316c88f5f3c4fdf6d4d168339efd43", null ],
    [ "ctrl_minimum", "structvid__devctrl__ctx.html#ab21e3376d30608121ddee5599ab9ecb4", null ],
    [ "ctrl_name", "structvid__devctrl__ctx.html#a388c9359e9a563c0b2a0a783800a7cf4", null ],
    [ "ctrl_newval", "structvid__devctrl__ctx.html#abcc0284f44e788f8d8ef25643919544a", null ],
    [ "ctrl_type", "structvid__devctrl__ctx.html#a596f992aa7501e10094b67cb0d053d47", null ]
];