var indexSectionsWithContent =
{
  0: "abcdefghijlmnoprstuvwxy",
  1: "abcdefhijmnprstuvwx",
  2: "c",
  3: "cdefjlmnprstvw",
  4: "cdeghijmnoprstvwxy",
  5: "abcdeghirstuvw",
  6: "e",
  7: "dr"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "defines",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Macros",
  7: "Pages"
};

