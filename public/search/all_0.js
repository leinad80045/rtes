var searchData=
[
  ['annotate_5fstring_0',['annotate_string',['../structraspicam__camera__parameters__s.html#a580b981c1118c3abb2b79ed2113dc00d',1,'raspicam_camera_parameters_s']]],
  ['annotate_5ftext_5fsize_1',['annotate_text_size',['../structraspicam__camera__parameters__s.html#af3f1b461d17f55ec874e1063a11b9e5c',1,'raspicam_camera_parameters_s']]],
  ['auth_5fparam_2',['auth_param',['../structauth__param.html',1,'']]],
  ['awb_5fgains_5fb_3',['awb_gains_b',['../structraspicam__camera__parameters__s.html#a8a90bcbcf60e97a9f665f74e7bbd2802',1,'raspicam_camera_parameters_s']]],
  ['awb_5fgains_5fr_4',['awb_gains_r',['../structraspicam__camera__parameters__s.html#aed49fa0b87ef885188c92db25123d307',1,'raspicam_camera_parameters_s']]]
];
