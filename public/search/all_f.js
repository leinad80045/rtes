var searchData=
[
  ['readme_123',['README',['../md__c_1__users__daniel__documents__workspace__r_t_e_s__r_e_a_d_m_e.html',1,'']]],
  ['r_124',['R',['../classandroidx_1_1legacy_1_1coreui_1_1_r.html',1,'androidx.legacy.coreui.R'],['../classandroidx_1_1localbroadcastmanager_1_1_r.html',1,'androidx.localbroadcastmanager.R'],['../classandroidx_1_1print_1_1_r.html',1,'androidx.print.R'],['../classandroidx_1_1slidingpanelayout_1_1_r.html',1,'androidx.slidingpanelayout.R'],['../classandroidx_1_1constraintlayout_1_1widget_1_1_r.html',1,'androidx.constraintlayout.widget.R'],['../classcom_1_1example_1_1rescu__e__interface_1_1_r.html',1,'com.example.rescu_e_interface.R'],['../classandroidx_1_1documentfile_1_1_r.html',1,'androidx.documentfile.R'],['../classandroidx_1_1appcompat_1_1_r.html',1,'androidx.appcompat.R'],['../classandroidx_1_1swiperefreshlayout_1_1_r.html',1,'androidx.swiperefreshlayout.R'],['../classandroidx_1_1core_1_1_r.html',1,'androidx.core.R'],['../classandroidx_1_1lifecycle_1_1livedata_1_1_r.html',1,'androidx.lifecycle.livedata.R'],['../classandroidx_1_1lifecycle_1_1_r.html',1,'androidx.lifecycle.R'],['../classandroidx_1_1coordinatorlayout_1_1_r.html',1,'androidx.coordinatorlayout.R'],['../classandroidx_1_1vectordrawable_1_1_r.html',1,'androidx.vectordrawable.R'],['../classandroidx_1_1viewpager_1_1_r.html',1,'androidx.viewpager.R'],['../classandroidx_1_1versionedparcelable_1_1_r.html',1,'androidx.versionedparcelable.R'],['../classandroidx_1_1fragment_1_1_r.html',1,'androidx.fragment.R'],['../classandroidx_1_1interpolator_1_1_r.html',1,'androidx.interpolator.R'],['../classandroidx_1_1drawerlayout_1_1_r.html',1,'androidx.drawerlayout.R'],['../classandroidx_1_1asynclayoutinflater_1_1_r.html',1,'androidx.asynclayoutinflater.R'],['../classandroidx_1_1customview_1_1_r.html',1,'androidx.customview.R'],['../classandroidx_1_1cursoradapter_1_1_r.html',1,'androidx.cursoradapter.R'],['../classandroidx_1_1lifecycle_1_1livedata_1_1core_1_1_r.html',1,'androidx.lifecycle.livedata.core.R'],['../classandroidx_1_1loader_1_1_r.html',1,'androidx.loader.R'],['../classandroidx_1_1arch_1_1core_1_1_r.html',1,'androidx.arch.core.R'],['../classandroidx_1_1legacy_1_1coreutils_1_1_r.html',1,'androidx.legacy.coreutils.R'],['../classandroidx_1_1lifecycle_1_1viewmodel_1_1_r.html',1,'androidx.lifecycle.viewmodel.R']]],
  ['raspicam_5fcamera_5fparameters_5fs_125',['raspicam_camera_parameters_s',['../structraspicam__camera__parameters__s.html',1,'']]],
  ['raspicli_2ec_126',['RaspiCLI.c',['../_raspi_c_l_i_8c.html',1,'']]],
  ['raspicli_5fdisplay_5fhelp_127',['raspicli_display_help',['../_raspi_c_l_i_8c.html#a31311751947144c959ed352431f574ea',1,'RaspiCLI.c']]],
  ['raspicli_5fget_5fcommand_5fid_128',['raspicli_get_command_id',['../_raspi_c_l_i_8c.html#a06c0dc687ddb9a0549e7ccdfe9d08a52',1,'RaspiCLI.c']]],
  ['raspicli_5fmap_5fxref_129',['raspicli_map_xref',['../_raspi_c_l_i_8c.html#a1d6c985c5efe4037911aab68b8fcb811',1,'RaspiCLI.c']]],
  ['raspicli_5funmap_5fxref_130',['raspicli_unmap_xref',['../_raspi_c_l_i_8c.html#a0ad59097e6499a2085bbcf842e9047b9',1,'RaspiCLI.c']]],
  ['rbuf_131',['rbuf',['../structrbuf.html',1,'']]],
  ['rbuf_5fflush_132',['rbuf_flush',['../netcam__wget_8c.html#a7df53d1f6a871e465c886d64caa09cd2',1,'netcam_wget.c']]],
  ['rbuf_5finitialize_133',['rbuf_initialize',['../netcam__wget_8c.html#a5078c4d589a73ecdd4888421280bcce6',1,'netcam_wget.c']]],
  ['rbuf_5fpeek_134',['rbuf_peek',['../netcam__wget_8c.html#aeb6bcf10a15cd4e5861e8aa9ebd59955',1,'netcam_wget.c']]],
  ['read_5fcamera_5fdir_135',['read_camera_dir',['../conf_8c.html#aa201227a6f5d7db724a3b5894f86012c',1,'conf.c']]],
  ['rescue_136',['rescue',['../motion_8c.html#ae6f9d42d552ccf6d60a30e98b2906000',1,'motion.c']]],
  ['rescuecall_137',['rescueCall',['../motion_8c.html#a94b37e399b48e9a3dc06cdd8837eb2f7',1,'motion.c']]],
  ['restart_138',['restart',['../motion_8c.html#a826d615f37cdd032eb80c92ffcc78120',1,'motion.c']]],
  ['roi_139',['roi',['../structraspicam__camera__parameters__s.html#a8813abc84420f2436c4b2f4e6d230e89',1,'raspicam_camera_parameters_s']]],
  ['rotate_2ec_140',['rotate.c',['../rotate_8c.html',1,'']]],
  ['rotate_5fdeinit_141',['rotate_deinit',['../rotate_8c.html#a9ede56383a6385a9d1150a9e7da21a32',1,'rotate.c']]],
  ['rotate_5finit_142',['rotate_init',['../rotate_8c.html#aecbab6202a9f06395c780f899f8637d8',1,'rotate.c']]],
  ['rotate_5fmap_143',['rotate_map',['../rotate_8c.html#aa314c557f7807657adba66468ae0743a',1,'rotate.c']]],
  ['rotdata_144',['rotdata',['../structrotdata.html',1,'']]],
  ['rtsp_5fcontext_145',['rtsp_context',['../structrtsp__context.html',1,'']]]
];
