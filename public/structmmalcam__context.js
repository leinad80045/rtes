var structmmalcam__context =
[
    [ "camera_buffer_pool", "structmmalcam__context.html#a65ad0ac1f243e60b2463c52a06556683", null ],
    [ "camera_buffer_queue", "structmmalcam__context.html#a0d44bece38169378598309dd432ecf3a", null ],
    [ "camera_capture_port", "structmmalcam__context.html#a2ebc268d1bf9412e0901fa198d846748", null ],
    [ "camera_component", "structmmalcam__context.html#af25a99cdcf67ab9044f19555d61ad21f", null ],
    [ "camera_parameters", "structmmalcam__context.html#ab11fd8fda4265cbff83695068d136dbe", null ],
    [ "cnt", "structmmalcam__context.html#abcf45c9ec6e9c29de1a4c8ac7dd527c3", null ],
    [ "framerate", "structmmalcam__context.html#a519ab16cc579e3dc27874bc473c9fb31", null ],
    [ "height", "structmmalcam__context.html#a92947484ad62d7af7b5bcb2accf70490", null ],
    [ "width", "structmmalcam__context.html#a0187ea37c86f15f8e25622fa95f4c754", null ]
];