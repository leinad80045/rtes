var structconfig =
[
    [ "area_detect", "structconfig.html#a791e6a3c5cd2448b3f36e52625183790", null ],
    [ "argc", "structconfig.html#a348435a3fa18bc9b84059f2fe757a01d", null ],
    [ "argv", "structconfig.html#a8addaf718a898fbca407cfa6c3ef45f3", null ],
    [ "auto_brightness", "structconfig.html#aa4b1993a7ede38b6de9ab25e59c80f9f", null ],
    [ "camera_dir", "structconfig.html#aa47912b4cbcf5dfa97b9faa0645fca78", null ],
    [ "camera_id", "structconfig.html#a1abb5d8ba8a5ff16793012c2db5c88db", null ],
    [ "camera_name", "structconfig.html#a4dc373ee9acb9e51c3656932c8c8cf28", null ],
    [ "database_busy_timeout", "structconfig.html#aabd598bc49a667fd763047acc7ce711f", null ],
    [ "database_dbname", "structconfig.html#a6ef9503997ce46f13a19efc26ca5730e", null ],
    [ "database_host", "structconfig.html#a20e3f4db45818264fd090d0c655a552d", null ],
    [ "database_password", "structconfig.html#a301b7ca90da69b480fe62f6af6098a07", null ],
    [ "database_port", "structconfig.html#a7b3f78a536e79f75ab15a1d7aad54cbe", null ],
    [ "database_type", "structconfig.html#a2cad144a2aa53e990bd8194391ec4841", null ],
    [ "database_user", "structconfig.html#a95271a4f9e749edeb7983c6d2c5fe997", null ],
    [ "despeckle_filter", "structconfig.html#a1e0bcfac586343f2f185a7e8f580349f", null ],
    [ "emulate_motion", "structconfig.html#a69ab46d0e639552a20d7c17d522bc131", null ],
    [ "event_gap", "structconfig.html#a8175fbb89963394af9fa04790fcf0e9f", null ],
    [ "flip_axis", "structconfig.html#ae19f58e3269f53163a2c0010732b41c6", null ],
    [ "framerate", "structconfig.html#a87841c2fa0bf5bf7b94bea0f957e3397", null ],
    [ "frequency", "structconfig.html#a7d9bc62f45fdb7f4a8b1cac60f95d5e6", null ],
    [ "height", "structconfig.html#aab40dedb7ff74be2362dfce1fe773006", null ],
    [ "input", "structconfig.html#a51df8aa93b78f00acb28c7cff7326025", null ],
    [ "lightswitch_frames", "structconfig.html#a7c98e8b46958f977fa336d72086934bc", null ],
    [ "lightswitch_percent", "structconfig.html#a0616600bdcedcdd1ac879765542761f6", null ],
    [ "locate_motion_mode", "structconfig.html#ab499eb968fa86b740f11344bc652c13a", null ],
    [ "locate_motion_style", "structconfig.html#ab7742822ed5afc2fa868c3d6d7bec5e5", null ],
    [ "log_file", "structconfig.html#af47ae3e2b1ac0f5d94f40ccb26f2628c", null ],
    [ "log_level", "structconfig.html#abf39954280279dfa7e757da3a1df4062", null ],
    [ "log_type", "structconfig.html#a5228e117feda7dd17ab470fcf5d91550", null ],
    [ "mask_file", "structconfig.html#aeb75a53962de71fd4b9ef72a9a79df1b", null ],
    [ "mask_privacy", "structconfig.html#a0d580e54e243a495ed7206367becc716", null ],
    [ "minimum_frame_time", "structconfig.html#a20ae8a03a4db588e99f9cddeecf0db5e", null ],
    [ "minimum_motion_frames", "structconfig.html#ad545894b30cdde3f1c5462c97d3cb4d6", null ],
    [ "mmalcam_control_params", "structconfig.html#aa72ff13cea34c6c15f5e5f2a02856ff3", null ],
    [ "mmalcam_name", "structconfig.html#acf71c1ee2ae0b3fdce38e5d00f64a0ee", null ],
    [ "movie_bps", "structconfig.html#a2cc3bb4744805dd98646c2e731839724", null ],
    [ "movie_codec", "structconfig.html#a01fa379d4b1e8fb4dfb7306d38e38915", null ],
    [ "movie_duplicate_frames", "structconfig.html#a6bb799b9e7f91afb08cd46513a8c88fb", null ],
    [ "movie_extpipe", "structconfig.html#a75f32c2c193742e29509ceb47bdc5f3b", null ],
    [ "movie_extpipe_use", "structconfig.html#ae9ff0fa896839b7148166ace6892516a", null ],
    [ "movie_filename", "structconfig.html#a667c1a051c4abbb9c0cfbb397c0964b5", null ],
    [ "movie_max_time", "structconfig.html#a437ee735f14d5b8b261dc29790c3f608", null ],
    [ "movie_output", "structconfig.html#a005aa2d88309b744b591232694f3b967", null ],
    [ "movie_output_motion", "structconfig.html#af1affc2123c965a5b6f276a9968c716b", null ],
    [ "movie_passthrough", "structconfig.html#a87d74f8a63594668ebbf0a1a05d62b81", null ],
    [ "movie_quality", "structconfig.html#a2ab8a828e3e1f4b4152f99467aae6ec8", null ],
    [ "native_language", "structconfig.html#a32ce24068bddf9e5397401b60d60d73e", null ],
    [ "netcam_decoder", "structconfig.html#a302abc803692982cb4522b0fd9570818", null ],
    [ "netcam_highres", "structconfig.html#abd32a2d5f3c9baacbbc9bc8327ba98f3", null ],
    [ "netcam_keepalive", "structconfig.html#a7fbed5589366d650afcbb8f298ad7af3", null ],
    [ "netcam_proxy", "structconfig.html#a228edf04fca38d5c7f1146ded6e7f5bf", null ],
    [ "netcam_tolerant_check", "structconfig.html#a5f8cc45923a3abb8189c3a7ce7f7d505", null ],
    [ "netcam_url", "structconfig.html#a9f1dd9dfe070bd2ddb6b7d6c415f19e3", null ],
    [ "netcam_use_tcp", "structconfig.html#a97b668ae1dbf536d71a9016334b92d2a", null ],
    [ "netcam_userpass", "structconfig.html#a0bf4433ecc54a3d346b949f56c38d015", null ],
    [ "noise_level", "structconfig.html#a2f04abd2f1dd9e3d5b4c14a79ab34cab", null ],
    [ "noise_tune", "structconfig.html#a453447bfc90b932d55c289701b03b792", null ],
    [ "norm", "structconfig.html#a3ac0235ce9547b82fff7fce0367416eb", null ],
    [ "on_area_detected", "structconfig.html#a8ce669fff3464a05b9afce0cda5494e4", null ],
    [ "on_camera_found", "structconfig.html#a8862a06cd4cd0af7f06c68ab0ff05979", null ],
    [ "on_camera_lost", "structconfig.html#a3c0a4026d204db7c3c7f48832ab42777", null ],
    [ "on_event_end", "structconfig.html#aa666a0d3d3dbb1d2990422f743c4825a", null ],
    [ "on_event_start", "structconfig.html#ad2e96ae6b24fecfa887bf4ad097aaea8", null ],
    [ "on_motion_detected", "structconfig.html#af831ffd7a2e71e7d7bfec2d6aefdc923", null ],
    [ "on_movie_end", "structconfig.html#a8190f26ea7a55564cc0fbdf3b81298b8", null ],
    [ "on_movie_start", "structconfig.html#ad1c6cc3aa5ea9636a59668f55437d098", null ],
    [ "on_picture_save", "structconfig.html#abb198a955123a42659ec0cddbf7538a7", null ],
    [ "picture_exif", "structconfig.html#a286dfa938560baf8a349e2d620e26159", null ],
    [ "picture_filename", "structconfig.html#a969cc3db6f37824107153ff13922e440", null ],
    [ "picture_output", "structconfig.html#ae1100d370a06ae1db001201ff94adc93", null ],
    [ "picture_output_motion", "structconfig.html#a4cfc76f50d7d9918e3fa25d1393d3245", null ],
    [ "picture_quality", "structconfig.html#a56f323078e46893cd013a208e171bdbd", null ],
    [ "picture_type", "structconfig.html#a152c161f08eba6b890b467dfa2636cc3", null ],
    [ "pid_file", "structconfig.html#aa9ebe790184162392f31468eb9cd2c1a", null ],
    [ "post_capture", "structconfig.html#a08676c91ffd1ea0e7815166ff4d45c1b", null ],
    [ "pre_capture", "structconfig.html#a5b4964167457f7648ebdf5bd2299584c", null ],
    [ "quiet", "structconfig.html#a22a7e16ddcfcdb5bf85b9320c93e3d76", null ],
    [ "rotate", "structconfig.html#a0de3c418750d190cc5b853649f8d1fbb", null ],
    [ "roundrobin_frames", "structconfig.html#ade59e66638f78bcc6935da23f568bf43", null ],
    [ "roundrobin_skip", "structconfig.html#a83e44a8bf53d41d8df4ea2070f4413a6", null ],
    [ "roundrobin_switchfilter", "structconfig.html#a54b92f98eeaa6f77775a3c6a08c02fd6", null ],
    [ "setup_mode", "structconfig.html#a3d30e8891d8676d40d5a399e03b28e49", null ],
    [ "smart_mask_speed", "structconfig.html#ad0f2e39e515116484d1d1414055d2b55", null ],
    [ "snapshot_filename", "structconfig.html#a482ad44d9e957258c30194c8bfe64e90", null ],
    [ "snapshot_interval", "structconfig.html#afd1cc8f5d3a60df91b17e0466fd779f9", null ],
    [ "sql_log_movie", "structconfig.html#a8706d80f15980a8ac9384aed5940486a", null ],
    [ "sql_log_picture", "structconfig.html#aa976411a2cac29b4493daead295aa474", null ],
    [ "sql_log_snapshot", "structconfig.html#a83923c434f509f3b9a57aae86d58983d", null ],
    [ "sql_log_timelapse", "structconfig.html#ad7329712f4087ed6856259b2e64429e4", null ],
    [ "sql_query", "structconfig.html#a1e73f7848cc8a3aa79a69cbf116b8157", null ],
    [ "sql_query_start", "structconfig.html#a70e3c2d6a9085c8f00a1b08468560569", null ],
    [ "sql_query_stop", "structconfig.html#ac2ff6cc1e665db5c75fa597863a57161", null ],
    [ "stream_auth_method", "structconfig.html#a32b8154e023b34251bc8e5c9200acfe9", null ],
    [ "stream_authentication", "structconfig.html#ae83ccd51be23a34c1ec9dc4d4ad180cc", null ],
    [ "stream_cors_header", "structconfig.html#ac16b1e2ab95831071d9787696602ecbf", null ],
    [ "stream_grey", "structconfig.html#ab39d64398f5fe14ab632e03eccdcb5e6", null ],
    [ "stream_limit", "structconfig.html#ac0a4b59f771713c5a6b074339177d118", null ],
    [ "stream_localhost", "structconfig.html#a293efa12181eeafb18d5eb23e521f221", null ],
    [ "stream_maxrate", "structconfig.html#aa8b6468f39e6c77b227d16f2eaeb7b22", null ],
    [ "stream_motion", "structconfig.html#a7d1d0ea6c5e2ae4785cae271acf64a88", null ],
    [ "stream_port", "structconfig.html#ab0c0d10750f786618a669b04999b5198", null ],
    [ "stream_preview_method", "structconfig.html#ae71636b09929f5cc7d29a81ccdd48b89", null ],
    [ "stream_preview_newline", "structconfig.html#a3431678d688d735ac34fecf90d73a5a1", null ],
    [ "stream_preview_scale", "structconfig.html#a66233a886a4153e3c0e9ba355989affd", null ],
    [ "stream_quality", "structconfig.html#a6d195cefd5d7383a6ba93464be671f43", null ],
    [ "stream_tls", "structconfig.html#abaa1b6e1839a2f608ba0e106a2ea1b42", null ],
    [ "target_dir", "structconfig.html#ab3a028a5acab4f89603b9f4889108ac0", null ],
    [ "text_changes", "structconfig.html#ae962261dfb9841523dfc56da84800d71", null ],
    [ "text_event", "structconfig.html#a506b3e1045a12e8be4dd7081897bed63", null ],
    [ "text_left", "structconfig.html#aec2e6e906c66cfa7f473e438654c3048", null ],
    [ "text_right", "structconfig.html#a8975af064fee45ff449fa4c1afc3327b", null ],
    [ "text_scale", "structconfig.html#a80c611aa626a31b0b4bf155b733b76ca", null ],
    [ "threshold", "structconfig.html#afcffebb0b974ebcae9698cf4985197b2", null ],
    [ "threshold_maximum", "structconfig.html#a76ee92759971c9f15da8a07e458dd54f", null ],
    [ "threshold_tune", "structconfig.html#a5b8fb73cd1931a2a879c1b3118f5491e", null ],
    [ "timelapse_codec", "structconfig.html#a2924bd2465832be1e89bd41111821586", null ],
    [ "timelapse_filename", "structconfig.html#aef2cd14da909e0750a3cc9f7e1c21097", null ],
    [ "timelapse_fps", "structconfig.html#a11f7b59fc3e95985b848706d578fbf88", null ],
    [ "timelapse_interval", "structconfig.html#abbbc8cb97592becf7d0ef61b0cf1ff0a", null ],
    [ "timelapse_mode", "structconfig.html#a9aa0267e2fd032802b251cf0e1151413", null ],
    [ "tuner_device", "structconfig.html#ab5a9a9cd6b717e6b71317bad65e86e47", null ],
    [ "v4l2_palette", "structconfig.html#a405fb536b10351f5f4d1a076b35c42d2", null ],
    [ "vid_control_params", "structconfig.html#ae86cb2ded7d5fc9581a8a5227bf93ee3", null ],
    [ "video_device", "structconfig.html#ae46c7cee52192d7024141fd9518369b6", null ],
    [ "video_pipe", "structconfig.html#a2af9da875eae16aeb9256744068e7cab", null ],
    [ "video_pipe_motion", "structconfig.html#a0b091a3bd13d9ca6517d5d35df5ec4e7", null ],
    [ "webcontrol_auth_method", "structconfig.html#a08d072f4c34f19c01c3041ccf9d34e7a", null ],
    [ "webcontrol_authentication", "structconfig.html#a59cd4fbfcc4ca0d23ad41f8ab4658907", null ],
    [ "webcontrol_cert", "structconfig.html#aad324a2955953be6454aaa2c98284513", null ],
    [ "webcontrol_cors_header", "structconfig.html#aa157ff4061d74130e73c69e2d185a5a1", null ],
    [ "webcontrol_interface", "structconfig.html#ac92fc24b0c5137cb3aff3a5121da2ca2", null ],
    [ "webcontrol_ipv6", "structconfig.html#a358b99624f37eeea3e3ac0230fed6f53", null ],
    [ "webcontrol_key", "structconfig.html#a4c784aa12ab186d40604dd2b6b09b31b", null ],
    [ "webcontrol_localhost", "structconfig.html#a3b7087fbe1147f21e77d83175666dc69", null ],
    [ "webcontrol_parms", "structconfig.html#a8b2846cba5af597b6928c94527c96bc6", null ],
    [ "webcontrol_port", "structconfig.html#a073381aa9c8f641bd977920cd76a0e34", null ],
    [ "webcontrol_tls", "structconfig.html#adfcdfa324540d1f8f9c713b296334b7a", null ],
    [ "width", "structconfig.html#a0cac88e677becf00ba13e9179c2870f5", null ]
];