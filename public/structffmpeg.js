var structffmpeg =
[
    [ "base_pts", "structffmpeg.html#aa3ea8bc851af20ec68f5cdaecb27208a", null ],
    [ "bps", "structffmpeg.html#afcd13a1a7bba1b6d6263898c525d5055", null ],
    [ "codec_name", "structffmpeg.html#a60d414f4124656ff3c6feef69cc4020e", null ],
    [ "filename", "structffmpeg.html#a5f0517e09f4b3c23328a2688353baa55", null ],
    [ "fps", "structffmpeg.html#a4bb083daede4d969c3d765dfe2030ae3", null ],
    [ "gop_cnt", "structffmpeg.html#a99245c214e4df746fd15e5e61d36f05b", null ],
    [ "height", "structffmpeg.html#a531aff53dd80d6104ed46398489c7b8a", null ],
    [ "high_resolution", "structffmpeg.html#ab35c657e0bd99d32ced49c995510d3a9", null ],
    [ "last_pts", "structffmpeg.html#afc34ba18d9c5f7f6b1ee866cbadaf72e", null ],
    [ "motion_images", "structffmpeg.html#a43edd854ff045e9f315da64044b25e5c", null ],
    [ "passthrough", "structffmpeg.html#a6b1c90e6648591492ac7a826d8c30940", null ],
    [ "quality", "structffmpeg.html#a9083f85511b8b50791efe2c1cf366a67", null ],
    [ "rtsp_data", "structffmpeg.html#a1bd77ecab26e5f5990176679b37ecd62", null ],
    [ "start_time", "structffmpeg.html#ad98ad12a5c83d83075722b00f666baa8", null ],
    [ "test_mode", "structffmpeg.html#a14196ab952d53f469258f8c0551a3aa1", null ],
    [ "tlapse", "structffmpeg.html#afcf7f340289f89a0dac25269bd14b9bd", null ],
    [ "width", "structffmpeg.html#af83bee3d0ad410d1db50283547ccc92c", null ]
];