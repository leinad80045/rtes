var jpegutils_8c =
[
    [ "jpgutl_error_mgr", "structjpgutl__error__mgr.html", "structjpgutl__error__mgr" ],
    [ "mem_destination_mgr", "structmem__destination__mgr.html", "structmem__destination__mgr" ],
    [ "mem_dest_ptr", "jpegutils_8c.html#acaa24add62b9c0703e4f4181d37d2ae5", null ],
    [ "jpgutl_decode_jpeg", "jpegutils_8c.html#a7e1cab3dcfa45bf7549c72a13d657ef8", null ],
    [ "jpgutl_put_grey", "jpegutils_8c.html#a96507b82f7ecc638b6584c67f4b5f1f2", null ],
    [ "jpgutl_put_yuv420p", "jpegutils_8c.html#ae0bd4bf96aee27e3e5d1c71bf7f92bba", null ],
    [ "METHODDEF", "jpegutils_8c.html#a1a64d09a8eaa504dd788c52dfb3cf6d6", null ],
    [ "METHODDEF", "jpegutils_8c.html#a2a2c08a6aa3b689bbc62c44d92a00091", null ]
];