var structimage__data =
[
    [ "cent_dist", "structimage__data.html#aaedf5937dd309c8fee820f1162e0e5b6", null ],
    [ "diffs", "structimage__data.html#af032561872b9767793b5044797d35d6c", null ],
    [ "flags", "structimage__data.html#a740b99265010d24828ea0fd8908e399b", null ],
    [ "idnbr_high", "structimage__data.html#a27fe844c92128fe8114bed6f61027d37", null ],
    [ "idnbr_norm", "structimage__data.html#a003d48d12e7422a57364ed4ef8a2be1c", null ],
    [ "image_high", "structimage__data.html#a8457f455b70c352b8bb1fb9254aecce0", null ],
    [ "image_norm", "structimage__data.html#aa90af31a1fdfdfda35c7c6b8f4147591", null ],
    [ "location", "structimage__data.html#a70d342ffd9da6cdf16a2bd97d57083fb", null ],
    [ "shot", "structimage__data.html#a7bd9da9681a650956ff9680b9aac5f04", null ],
    [ "timestamp_tv", "structimage__data.html#a25f026f0fc9a05731986d38958589885", null ],
    [ "total_labels", "structimage__data.html#adcf54bba046c2793c0fc7011b6dc7d1c", null ]
];