var wheels_8c =
[
    [ "backwards", "wheels_8c.html#a260723caa5542c3c87e620deaeca3f18", null ],
    [ "gpio", "wheels_8c.html#a052f1a3f1cd518c265315c251b7ce9c9", null ],
    [ "left", "wheels_8c.html#a428f8207615465afdfcf1d31547ffef3", null ],
    [ "outputs", "wheels_8c.html#a3646d455d3c9526f6fd360feab19fe77", null ],
    [ "right", "wheels_8c.html#ae545bf658b2c876abbbae55a7d12875f", null ],
    [ "stio", "wheels_8c.html#a1cffe05e142828f9beea218d1238f541", null ],
    [ "towards", "wheels_8c.html#acec95f92c2e44ab9a0b21ad92298c2e0", null ],
    [ "moveBackwards", "wheels_8c.html#a3f300838257ec2503b2361f7c62a9c93", null ],
    [ "moveTowards", "wheels_8c.html#a36d1a7f43a2bc725ec01242e9d612fb0", null ],
    [ "stop", "wheels_8c.html#a8c528baf37154d347366083f0f816846", null ],
    [ "turnLeft", "wheels_8c.html#adaf487f84c38e060c84f3cb829e70f2b", null ],
    [ "turnRight", "wheels_8c.html#acf4fa5da14085c3a9a170f9de29d2755", null ],
    [ "wheels", "wheels_8c.html#a6d960ab1e3e799e4fc4f052467356fe8", null ]
];