var logger_8c =
[
    [ "get_log_level_str", "logger_8c.html#ade0ce36a1248bad55d0bce0b99fd728e", null ],
    [ "get_log_type", "logger_8c.html#a9be2f5197121ab94091e813f4ce56a8e", null ],
    [ "get_log_type_str", "logger_8c.html#a4f25a5960c95d4be7d146f43dd3aa98c", null ],
    [ "motion_log", "logger_8c.html#a637310d6590938e94a7a3a61e3379488", null ],
    [ "set_log_level", "logger_8c.html#a4001b2170899f8c37f4676150fa6abef", null ],
    [ "set_log_mode", "logger_8c.html#af84d1fe3ae512d2481029c0571adaa02", null ],
    [ "set_log_type", "logger_8c.html#a5a8c19d81ec747552dc0499924a7bb1e", null ],
    [ "set_logfile", "logger_8c.html#a5737a913850a35cc58e84ca9fd6b92aa", null ]
];