var structmhdstart__ctx =
[
    [ "cnt", "structmhdstart__ctx.html#a9269430acf69071ae07265f887cece24", null ],
    [ "ctrl", "structmhdstart__ctx.html#a96d90ac1893051cc033b812eafd8e413", null ],
    [ "indxthrd", "structmhdstart__ctx.html#a64e0488a86b136120e26511d0acae150", null ],
    [ "ipv6", "structmhdstart__ctx.html#acaef074a5704673e313628c2af2daf01", null ],
    [ "lpbk_ipv4", "structmhdstart__ctx.html#a6dbefbaf2c8469efb77247d6ef6c3e18", null ],
    [ "lpbk_ipv6", "structmhdstart__ctx.html#a685197c6cac903387f6d752a687b1ff5", null ],
    [ "mhd_flags", "structmhdstart__ctx.html#aa3d1cd91dfabe90aa3f0e29c3b3f179a", null ],
    [ "mhd_ops", "structmhdstart__ctx.html#af623169b75cb62787e21987137fb0fc2", null ],
    [ "mhd_opt_nbr", "structmhdstart__ctx.html#abcbca5dba45de416248690c2e65d4752", null ],
    [ "tls_cert", "structmhdstart__ctx.html#a06932e2a61c19fea964ef12591a1b497", null ],
    [ "tls_key", "structmhdstart__ctx.html#a46f7367981e001d91b05ce1cd673c5b8", null ]
];