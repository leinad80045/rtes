var structftp__context =
[
    [ "control_buffer", "structftp__context.html#a4da69f951b2691191f5afc3c3ce9dd28", null ],
    [ "control_buffer_answer", "structftp__context.html#ab2950939e74d6e7be4c9d74915b9fc5f", null ],
    [ "control_buffer_index", "structftp__context.html#aa3414ef3846b44a86c4de08d47b05f37", null ],
    [ "control_buffer_used", "structftp__context.html#af610dcecbc35b82defcf89577dd254cb", null ],
    [ "control_file_desc", "structftp__context.html#a1234a9d8bab942172015f7a132c079b2", null ],
    [ "data_file_desc", "structftp__context.html#a83104ddb8fb049973d85fb7461cb22b4", null ],
    [ "ftp_address", "structftp__context.html#a2a1f27bf7e0d1a01078905f5b961dd47", null ],
    [ "passive", "structftp__context.html#a7b1b87941acb5c88847cbf0c9f9fd695", null ],
    [ "passwd", "structftp__context.html#ae6b18b5082238a1e67739485b65befab", null ],
    [ "path", "structftp__context.html#a35ebece4f7f7a558a22fc9a47bf078c9", null ],
    [ "returnValue", "structftp__context.html#af8cd318d6a186afd137a104114c31854", null ],
    [ "state", "structftp__context.html#af0f47608e8f0487e3bd067678f58d5fe", null ],
    [ "user", "structftp__context.html#a815cd8af6fb4d25e7807d5ad08b3b579", null ]
];